/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author hugoc
 */
public class Element implements Comparable<Element> {

    private int atomicNumber;
    private String element;
    private String symbol;
    private Double atomicWeight;
    private Double atomicMass;
    private int period;
    private int group;
    private String phase;
    private String mostStableCrystal;
    private String type;
    private Float ionicRadius;
    private Float atomicRadius;
    private Float electronegativity;
    private Float firstLonizationPotential;
    private Double density;
    private Float meltingPoint;
    private Float boilingPoint;
    private Integer isotopes;
    private String discoverer;
    private Integer yearOfDiscovery;
    private Float specificHeatCapacity;
    private String electronConfiguration;
    private int displayRow;
    private int displayColumn;

    public int getAtomicNumber() {
        return atomicNumber;
    }

    public void setAtomicNumber(int atomicNumber) {
        this.atomicNumber = atomicNumber;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Double getAtomicWeight() {
        return atomicWeight;
    }

    public void setAtomicWeight(Double atomicWeight) {
        this.atomicWeight = atomicWeight;
    }

    public Double getAtomicMass() {
        return atomicMass;
    }

    public void setAtomicMass(Double atomicMass) {
        this.atomicMass = atomicMass;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public String getMostStableCrystal() {
        return mostStableCrystal;
    }

    public void setMostStableCrystal(String mostStableCrystal) {
        this.mostStableCrystal = mostStableCrystal;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Float getIonicRadius() {
        return ionicRadius;
    }

    public void setIonicRadius(Float ionicRadius) {
        this.ionicRadius = ionicRadius;
    }

    public Float getAtomicRadius() {
        return atomicRadius;
    }

    public void setAtomicRadius(Float atomicRadius) {
        this.atomicRadius = atomicRadius;
    }

    public Float getElectronegativity() {
        return electronegativity;
    }

    public void setElectronegativity(Float electronegativity) {
        this.electronegativity = electronegativity;
    }

    public Float getFirstLonizationPotential() {
        return firstLonizationPotential;
    }

    public void setFirstLonizationPotential(Float firstLonizationPotential) {
        this.firstLonizationPotential = firstLonizationPotential;
    }

    public Double getDensity() {
        return density;
    }

    public void setDensity(Double density) {
        this.density = density;
    }

    public Float getMeltingPoint() {
        return meltingPoint;
    }

    public void setMeltingPoint(Float meltingPoint) {
        this.meltingPoint = meltingPoint;
    }

    public Float getBoilingPoint() {
        return boilingPoint;
    }

    public void setBoilingPoint(Float boilingPoint) {
        this.boilingPoint = boilingPoint;
    }

    public Integer getIsotopes() {
        return isotopes;
    }

    public void setIsotopes(Integer isotopes) {
        this.isotopes = isotopes;
    }

    public String getDiscoverer() {
        return discoverer;
    }

    public void setDiscoverer(String discoverer) {
        this.discoverer = discoverer;
    }

    public Integer getYearOfDiscovery() {
        return yearOfDiscovery;
    }

    public void setYearOfDiscovery(Integer yearOfDiscovery) {
        this.yearOfDiscovery = yearOfDiscovery;
    }

    public Float getSpecificHeatCapacity() {
        return specificHeatCapacity;
    }

    public void setSpecificHeatCapacity(Float specificHeatCapacity) {
        this.specificHeatCapacity = specificHeatCapacity;
    }

    public String getElectronConfiguration() {
        return electronConfiguration;
    }

    public void setElectronConfiguration(String electronConfiguration) {
        this.electronConfiguration = electronConfiguration;
    }

    public int getDisplayRow() {
        return displayRow;
    }

    public void setDisplayRow(int displayRow) {
        this.displayRow = displayRow;
    }

    public int getDisplayColumn() {
        return displayColumn;
    }

    public void setDisplayColumn(int displayColumn) {
        this.displayColumn = displayColumn;
    }

    private boolean canChangeOrder = true;
    private boolean orderByAtomicNumber = false;
    private boolean orderByElement = false;
    private boolean orderBySymbol = false;
    private boolean orderByAtomicMass = false;

    public boolean isOrderByAtomicNumber() {
        return orderByAtomicNumber;
    }

    private void setOrderByAtomicNumber() {
        if (canChangeOrder) {
            this.orderByAtomicNumber = true;

            this.orderByElement = false;
            this.orderBySymbol = false;
            this.orderByAtomicMass = false;
            this.canChangeOrder = false;
        }
    }

    public boolean isOrderByElement() {
        return orderByElement;
    }

    private void setOrderByElement() {
        if (canChangeOrder) {
            this.orderByElement = true;

            this.orderByAtomicNumber = false;
            this.orderBySymbol = false;
            this.orderByAtomicMass = false;
            this.canChangeOrder = false;
        }
    }

    public boolean isOrderBySymbol() {
        return orderBySymbol;
    }

    private void setOrderBySymbol() {
        if (canChangeOrder) {
            this.orderBySymbol = true;

            this.orderByAtomicNumber = false;
            this.orderByElement = false;
            this.orderByAtomicMass = false;
            this.canChangeOrder = false;
        }
    }

    public boolean isOrderByAtomicMass() {
        return orderByAtomicMass;
    }

    private void setOrderByAtomicMass() {
        if (canChangeOrder) {
            this.orderByAtomicMass = true;

            this.orderByAtomicNumber = false;
            this.orderByElement = false;
            this.orderBySymbol = false;
            this.canChangeOrder = false;
        }
    }

    public Element(boolean orderByAtomicNumber, boolean orderByElement, boolean orderBySymbol, boolean orderByAtomicMass) {
        if (orderByAtomicNumber) {
            setOrderByAtomicNumber();
        } else if (orderByElement) {
            setOrderByElement();
        } else if (orderBySymbol) {
            setOrderBySymbol();
        } else if (orderByAtomicMass) {
            setOrderByAtomicMass();
        } else {
            setOrderByAtomicNumber();
        }
    }

    @Override
    public int compareTo(Element o) {
        if (this.orderByElement) {
            return (element.compareTo(o.getElement()));
        }

        if (this.orderBySymbol) {
            return (symbol.compareTo(o.getSymbol()));
        }

        if (this.orderByAtomicMass) {
            return (atomicMass.compareTo(o.getAtomicMass()));
        }

        return (atomicNumber - o.getAtomicNumber());
    }

    @Override
    public String toString() {
        return "Element{" + "atomicNumber=" + atomicNumber + ", element=" + element + ", symbol=" + symbol + ", atomicMass=" + atomicMass + ", discoverer=" + discoverer + ", yearOfDiscovery=" + yearOfDiscovery + '}';
    }

}
