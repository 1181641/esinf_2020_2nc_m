/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import PL.BST;
import data.BSTExtend;
import data.BSTPeriodic;
import data.reader;
import data.searchAtomicMass;
import java.util.List;
import java.util.TreeMap;

/**
 *
 * @author hugoc
 */
public class Main {

    private static reader r;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        System.out.println("------------------ EX01 ------------------");
        // Read file and populate BST
        r = new reader();
        //System.out.println(r.getPeriodicTable().toString());
        //System.out.println(r.getPeriodicTable_element().toString());
        //System.out.println(r.getPeriodicTable_symbol().toString());
        //System.out.println(r.getPeriodicTable_atomicMass().toString());
        System.out.println("---------------- EX01 - A ----------------");
        //FIND Boron
        //System.out.println(r.getPeriodicTable().find((int) 5));
        //System.out.println(r.getPeriodicTable_element().find((String) "Boron"));
        //System.out.println(r.getPeriodicTable_symbol().find((String) "B"));
        //System.out.println(r.getPeriodicTable_atomicMass().find((Double) 10.81));
        System.out.println("---------------- EX01 - B ----------------");
        searchAtomicMass sam = r.getPeriodicTable_atomicMass().getAtomicMassBetween(20, 40);
        //for (Element item : sam.getElementos()) {
        //    System.out.println(item.toString());
        //}
        //for (String type : sam.getSumatorio().keySet()) {
        //    System.out.println("-----------------------------");
        //    System.out.println("Type: " + type);
        //    for (String phase : sam.getSumatorio().get(type).keySet()) {
        //        System.out.println("Phase: " + phase + " | count: " + sam.getSumatorio().get(type).get(phase));
        //    }
        //}
        //System.out.println("-----------------------------");

        System.out.println("------------------ EX02 ------------------");

        System.out.println("---------------- EX02 - A ----------------");
        TreeMap<Integer, List<String>> result = r.getPeriodicTable().ex2a(1);
        System.out.println("---------------- EX02 - B ----------------");
        BSTExtend bstResult = r.getPeriodicTable().ex2b(result);
        System.out.println("---------------- EX02 - C ----------------");
        r.getPeriodicTable().ex2c(bstResult);
        System.out.println("---------------- EX02 - D ----------------");
        result = r.getPeriodicTable().ex2a(0);
        if (result.containsKey(1)) {
            r.getPeriodicTable().ex2d(bstResult, result.get(1));
        }

    }
}
