/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import PL.BST;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import main.Element;

/**
 *
 * @author tiagosantos
 */
public class BSTPeriodic extends BST<Element> {

    @Override
    public void insert(Element element) {
        //super.insert(element); //To change body of generated methods, choose Tools | Templates.
        root = insert(element, root);
    }
    /**
     * Inserir o elemento na arvore quando o node não está preenchido.
     * @param element Elemento a ser inserido na arvore.
     * @param node Node onde o elemento será inserido caso seja null.
     * @return Retorna o node onde o elemento foi inserido
     */
    private Node<Element> insert(Element element, Node<Element> node) {

        if (node == null) {
            return new Node<Element>(element, null, null);
        }

        if (node.getElement().compareTo(element) < 0) {
            node.setRight(insert(element, node.getRight()));
        }

        if (node.getElement().compareTo(element) > 0) {
            node.setLeft(insert(element, node.getLeft()));
        }

        return node;
    }

    /**
     * Encontra um elemento conforme a configuração especificada na construção 
     * do objecto.
     *      AtomicNumber - Procura o elemento pelo o campo "AtomicNumber";
     *      Element - Procura o elemento pelo o campo "Element";
     *      Symbol - Procura o elemento pelo o campo "Symbol";
     *      AtomicMass - Procura o elemento pelo o campo "AtomicMass";
     * @param value Elemento a ser encontrado na arvore.
     * @return Retorna o elemento caso seja encontrado, senão devolve null.
     */
    public Element find(Object value) {
        if (isEmpty()) {
            return null;
        }

        if (root.getElement().isOrderByAtomicNumber()) {
            return findByAtomicNumber((int) value);
        }
        if (root.getElement().isOrderByElement()) {
            return findByElement((String) value);
        }
        if (root.getElement().isOrderBySymbol()) {
            return findBySymbol((String) value);
        }
        if (root.getElement().isOrderByAtomicMass()) {
            return findByAtomicMass((Double) value);
        }

        return null;
    }

    /**
     * Encontra um elemento no campo "AtomicNumber"
     * @param atomicNumber Elemento a ser encontrado na arvore 
     * @return Retorna o elemento caso seja encontrado, senão devolve null.
     */
    protected Element findByAtomicNumber(int atomicNumber) {
        if (isEmpty()) {
            return null;
        }

        Node<Element> node = root;
        boolean found = false;
        while (node != null && !found) {
            if (node.getElement().getAtomicNumber() == atomicNumber) {
                return node.getElement();
            }

            if (node.getElement().getAtomicNumber() > atomicNumber) {
                node = node.getLeft();
            }
            if (node != null && node.getElement().getAtomicNumber() < atomicNumber) {
                node = node.getRight();
            }
        }
        return null;
    }

    /**
     * Encontra um elemento no campo "Element"
     * @param element Elemento a ser encontrado na arvore
     * @return Retorna o elemento caso seja encontrado, senão devolve null.
     */
    protected Element findByElement(String element) {
        if (isEmpty()) {
            return null;
        }

        Node<Element> node = root;
        boolean found = false;
        while (node != null && !found) {
            if (node.getElement().getElement().equals(element)) {
                return node.getElement();
            }

            if (node.getElement().getElement().compareTo(element) > 0) {
                node = node.getLeft();
            }
            if (node != null && node.getElement().getElement().compareTo(element) < 0) {
                node = node.getRight();
            }
        }
        return null;
    }

    /**
     * Encontra um elemento no campo "Symbol"
     * @param symbol Elemento a ser encontrado na arvore
     * @return Retorna o elemento caso seja encontrado, senão devolve null.
     */
    protected Element findBySymbol(String symbol) {
        if (isEmpty()) {
            return null;
        }

        Node<Element> node = root;
        boolean found = false;
        while (node != null && !found) {
            if (node.getElement().getSymbol().equals(symbol)) {
                return node.getElement();
            }

            if (node.getElement().getSymbol().compareTo(symbol) > 0) {
                node = node.getLeft();
            }
            if (node != null && node.getElement().getSymbol().compareTo(symbol) < 0) {
                node = node.getRight();
            }
        }
        return null;
    }

    /**
     * Encontra um elemento no campo "AtomicMass"
     * @param atomicMass Elemento a ser encontrado na arvore
     * @return Retorna o elemento caso seja encontrado, senão devolve null.
     */
    protected Element findByAtomicMass(double atomicMass) {
        if (isEmpty()) {
            return null;
        }

        Node<Element> node = root;
        boolean found = false;
        while (node != null && !found) {
            if (node.getElement().getAtomicMass() == atomicMass) {
                return node.getElement();
            }

            if (node.getElement().getAtomicMass() > atomicMass) {
                node = node.getLeft();
            }
            if (node != null && node.getElement().getAtomicMass() < atomicMass) {
                node = node.getRight();
            }
        }
        return null;
    }

    /**
     * Retornar lista de elementos entre dois valores. Caso a BST não esteja
     * ordenada por AtomicMass retorna null
     *
     * @param min valor minimo
     * @param max valor maximo
     * @return Objeto com lista de elementos e mapa do sumatório das
     * propriedades type e phase
     */
    public searchAtomicMass getAtomicMassBetween(double min, double max) {
        if (!root.getElement().isOrderByAtomicMass()) {
            return null;
        }

        searchAtomicMass retValue = new searchAtomicMass();
        List<Element> lst = new ArrayList<>();
        Map<String, Map<String, Integer>> mapSumatorio = new TreeMap<>();

        Iterable<Element> i = inOrder();
        Iterator<Element> it = i.iterator();

        boolean ended = false;
        while (!ended && it.hasNext()) {
            Element myElement = it.next();

            if (myElement.getAtomicMass() > max) {
                ended = true;
            }
            if (myElement.getAtomicMass() >= min && myElement.getAtomicMass() <= max) {
                lst.add(myElement);

                // sumário dos elementos agrupados por type e phase
                if (myElement.getType() != null && myElement.getPhase() != null) {
                    Map<String, Integer> tmpMap = new TreeMap<>();
                    if (mapSumatorio.get(myElement.getType()) == null) {
                        tmpMap.put(myElement.getPhase(), 1);
                        tmpMap.put("total", 1);
                    } else {
                        tmpMap = mapSumatorio.get(myElement.getType());
                        Integer count = tmpMap.get(myElement.getPhase());
                        if (count == null) {
                            tmpMap.put(myElement.getPhase(), 1);
                        } else {
                            tmpMap.put(myElement.getPhase(), count + 1);
                        }
                        count = tmpMap.get("total");
                        tmpMap.put("total", count + 1);
                    }
                    mapSumatorio.put(myElement.getType(), tmpMap);
                }
            }
        }

        Comparator<Element> compareByYearOfDiscover;
        compareByYearOfDiscover = (Element o1, Element o2) -> {
            int v1 = (o1.getYearOfDiscovery() == null ? -1 : o1.getYearOfDiscovery());
            int v2 = (o2.getYearOfDiscovery() == null ? -1 : o2.getYearOfDiscovery());
            return (v2 - v1);
        };
        Comparator<Element> compareByDiscoverer = (Element o1, Element o2) -> o1.getDiscoverer().compareToIgnoreCase(o2.getDiscoverer());
        Collections.sort(lst, compareByYearOfDiscover);
        Collections.sort(lst, compareByDiscoverer);

        retValue.setElementos(lst);
        retValue.setSumatorio(mapSumatorio);

        return retValue;
    }

    /**
     * Recorrendo apenas à estrutura árvore binária de pesquisa (BST), devolva 
     * por ordem decrescente as configurações electrónicas com mais do que uma 
     * repetição, agrupadas por número de repetições. 
     * @param limit Limite minimo
     * @return Retorna um mapa de contadores por ordem decrescente com base no
     * limite minimo.
     */
    public TreeMap<Integer, List<String>> ex2a(int limit) {
        TreeMap<String, Integer> result = new TreeMap<>();
        ex2a(root, result);

        //CONVERT TREEMAP TO <INTEGER, STRING[]>
        TreeMap<Integer, List<String>> mapElectron = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        List<String> values;
        for (Map.Entry<String, Integer> item : result.entrySet()) {
            if (item.getValue() <= limit) {
                continue;
            }

            values = new ArrayList<String>();
            if (mapElectron.containsKey(item.getValue())) {
                values = mapElectron.get(item.getValue());
            }

            values.add(item.getKey());

            mapElectron.put(item.getValue(), values);
        }
        
        for (Map.Entry<Integer, List<String>> entry : mapElectron.entrySet()) {
            System.out.println(entry.getKey() + "   " + Arrays.toString(entry.getValue().toArray()));
        }
       
        return mapElectron;
    }

    /**
     * Recorrendo apenas à estrutura árvore binária de pesquisa (BST), devolva 
     * por ordem crescente as configurações electrónicas, agrupadas por número 
     * de repetições. 
     * @param node Node da arvore
     * @param result Mapa agrupado por numero de repetições.
     */
    private void ex2a(Node<Element> node, TreeMap<String, Integer> result) {
        if (node == null) {
            return;
        }

        String concatString = "";
        if (node.getElement().getElectronConfiguration() != null) {
            String[] electron = node.getElement().getElectronConfiguration().split(" ");
            for (String elect : electron) {
                if (concatString != "") {
                    concatString = concatString + " " + elect;
                } else {
                    concatString = elect;
                }

                if (result.containsKey(concatString)) {
                    result.put(concatString, result.get(concatString) + 1);
                } else {
                    result.put(concatString, 1);
                }
            }
        }
        if (node.getLeft() != null) {
            ex2a(node.getLeft(), result);
        }
        //
        if (node.getRight() != null) {
            ex2a(node.getRight(), result);
        }
    }

    /**
     * Constroi uma nova BST inserindo por ordem decrescente as configurações 
     * electrónicas com repetição acima de 2 obtidas na alínea anterior.
     * @param result Mapa agrupado por numero de repetições.
     * @return Nova arvore com base no mapa.
     */
    public BSTExtend ex2b (TreeMap<Integer, List<String>> result){
        BSTExtend retValue = new BSTExtend();
       
        for (Map.Entry<Integer, List<String>> entry : result.entrySet()) {
            for (String electron : entry.getValue()) {
                retValue.insert(electron);
            }
        }

        return retValue;
    }

    /**
     * Método que devolve os valores das duas configurações electrónicas 
     * mais distantes na árvore e a respectiva distância.
     * @param bstResult Arvore com nodes
     * @return Mapa com a distancia mais longa.
     */
    public Map<Integer, List<String>> ex2c(BSTExtend bstResult){
        if (bstResult == null)
            return null;
        
        Map<Integer, List<String>> retValue = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });

        Map<Integer, List<String>> levels = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        
        Object obj = bstResult.leafByLevel();
        if(obj == null)
            return null;
        
        levels.putAll((Map<Integer, List<String>>)obj);

        List<String> visited = new ArrayList<String>();
        List<String> elements;

        for (Map.Entry<Integer, List<String>> entry : levels.entrySet()) {
            for (String electron : entry.getValue()) {
                visited.add(electron);

                for (Map.Entry<Integer, List<String>> entry2 : levels.entrySet()) {
                    for (String electron2 : entry2.getValue()) {
                        if (visited.contains(electron2)) {
                            continue;
                        }

                        int distance = bstResult.findDistance(electron, electron2);
                        if (retValue.containsKey(distance)) {
                            elements = retValue.get(distance);
                        } else {
                            elements = new ArrayList<String>();
                        }

                        elements.add("D(" + electron + "," + electron2 + ")");
                        retValue.put(distance, elements);
                        if (retValue.size() > 1) {
                            int firstKey = (int) retValue.keySet().toArray()[0];
                            retValue.entrySet().removeIf(p -> p.getKey() < firstKey);
                        }
                    }
                }
            }
        }
        
        for (Map.Entry<Integer, List<String>> entry : retValue.entrySet()) {
            for(String item : entry.getValue()){
                System.out.println(item + " = " + entry.getKey());
            }
        }
        
        return retValue;
    }

    /**
     * Método que transforme a árvore obtida alínea anterior numa árvore 
     * binária completa, inserindo nesta possíveis configurações electrónicas únicas.
     * @param bstResult Arvore com nodes
     * @param electrons Lista de electrons a ser inserido caso a arvore não 
     * seja completa.
     * @return retorna verdadeiro caso a arvore seja completa e falso se a 
     * arvore for incompleta.
     */
    public boolean ex2d(BSTExtend bstResult, List<String> electrons){
        return bstResult.completeBstWithList(electrons);
    }
}
