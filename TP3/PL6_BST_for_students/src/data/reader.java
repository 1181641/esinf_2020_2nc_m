/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.Element;

/**
 *
 * @author (1181312) Hugo Costa | (1181641) Tiago Santos
 */
public class reader {

    private BSTPeriodic periodicTable = null;
    private BSTPeriodic periodicTable_element = null;
    private BSTPeriodic periodicTable_symbol = null;
    private BSTPeriodic periodicTable_atomicMass = null;

    /**
     * Metodo que retorna a arvore da tabela periodica
     * @return arvore da tabela periodica
     */
    public BSTPeriodic getPeriodicTable() {
        return periodicTable;
    }
    
    /**
     * Metodo que guarda a arvore da tabela periodica
     * @param periodicTable arvore da tabela periodica
     */
    public void setPeriodicTable(BSTPeriodic periodicTable) {
        this.periodicTable = periodicTable;
    }

    /**
     * Metodo que retorna a arvore da tabela periodica inserida com base no
     * campo "element"
     * @return arvore da tabela periodica
     */
    public BSTPeriodic getPeriodicTable_element() {
        return periodicTable_element;
    }

    /**
     * Metodo que guarda a arvore da tabela periodica inserida com base no
     * campo "element"
     * @param periodicTable_element arvore da tabela periodica
     */
    public void setPeriodicTable_element(BSTPeriodic periodicTable_element) {
        this.periodicTable_element = periodicTable_element;
    }

    /**
     * Metodo que retorna a arvore da tabela periodica inserida com base no 
     * campo "Symbol"
     * @return arvore da tabela periodica
     */
    public BSTPeriodic getPeriodicTable_symbol() {
        return periodicTable_symbol;
    }

    /**
     * Metodo que guarda a arvore da tabela periodica inserida com base no
     * campo "Symbol"
     * @param periodicTable_symbol arvore da tabela periodica
     */
    public void setPeriodicTable_symbol(BSTPeriodic periodicTable_symbol) {
        this.periodicTable_symbol = periodicTable_symbol;
    }

    /**
     * Metodo que retorna a arvore da tabela periodica inserida com base no
     * campo "AtomicMass"
     * @return arvore da tabela periodica
     */
    public BSTPeriodic getPeriodicTable_atomicMass() {
        return periodicTable_atomicMass;
    }

    /**
     * Metodo que guarda a arvore da tabela periodica inserida com base no
     * campo "AtomicMass"
     * @param periodicTable_atomicMass arvore da tabela periodica
     */
    public void setPeriodicTable_atomicMass(BSTPeriodic periodicTable_atomicMass) {
        this.periodicTable_atomicMass = periodicTable_atomicMass;
    }

    /**
     * Construtor da classe reader
     */
    public reader() {
        readPeriodicTable();
    }

    /**
     * Construtor da classe reader
     * @param periodicTable arvore de inicialização
     */
    public reader(BSTPeriodic periodicTable) {
        this.periodicTable = periodicTable;
    }

    /**
     * Ler elementos a partir do ficheiro fornecido e atribuir a "periodicTable"
     * Resolução de ex1.
     */
    private void readPeriodicTable() {
        File pathFile = new File(System.getProperty("user.dir"));
        pathFile = new File(pathFile, "src");
        pathFile = new File(pathFile, "data");
        File srcPath = new File(pathFile, "periodicTable.csv");
        String filePath = srcPath.getPath();
        BufferedReader br = null;
        String line = "";

        try {
            br = new BufferedReader(new FileReader(filePath));

            periodicTable = new BSTPeriodic();
            periodicTable_element = new BSTPeriodic();
            periodicTable_symbol = new BSTPeriodic();
            periodicTable_atomicMass = new BSTPeriodic();
            boolean isHeaderFirst = true;
            while ((line = br.readLine()) != null) {
                if (isHeaderFirst) {
                    isHeaderFirst = false;
                    continue;
                }
                String[] arrayData = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);

                Element newItem = newLine(arrayData, true, false, false, false);
                periodicTable.insert(newItem);

                Element newItemElement = newLine(arrayData, false, true, false, false);
                periodicTable_element.insert(newItemElement);

                Element newItemSymbol = newLine(arrayData, false, false, true, false);
                periodicTable_symbol.insert(newItemSymbol);

                Element newItemAtomicMass = newLine(arrayData, false, false, false ,true);
                periodicTable_atomicMass.insert(newItemAtomicMass);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(reader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | NumberFormatException ex) {
            Logger.getLogger(reader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo que constroi a estrutura Element para a mesma ser inserida na
     * arvore.
     * @param arrayData Vector de campos retornados no ficheiro csv
     * @param orderByAtomicNumber propriedade que identifica o tipo de procura
     * na arvore
     * @param orderByElement propriedade que identifica o tipo de procura
     * na arvore

     * @param orderBySymbol propriedade que identifica o tipo de procura
     * na arvore

     * @param orderByAtomicMass propriedade que identifica o tipo de procura
     * na arvore

     * @return Retorna a estrutura Element para ser inserido na arvore.
     */
    protected Element newLine(String[] arrayData, boolean orderByAtomicNumber, boolean orderByElement, boolean orderBySymbol, boolean orderByAtomicMass) {
        Element newItem = new Element(orderByAtomicNumber, orderByElement, orderBySymbol, orderByAtomicMass);
        
        newItem.setAtomicNumber(Integer.parseInt(arrayData[0].trim()));
        newItem.setElement(arrayData[1].trim());
        newItem.setSymbol(arrayData[2].trim());
        newItem.setAtomicWeight(Double.parseDouble(arrayData[3].trim()));
        newItem.setAtomicMass(Double.parseDouble(arrayData[4].trim()));
        newItem.setPeriod(Integer.parseInt(arrayData[5].trim()));
        newItem.setGroup(Integer.parseInt(arrayData[6].trim()));
        newItem.setPhase((arrayData[7].equals("") ? null : arrayData[7].trim()));
        newItem.setMostStableCrystal((arrayData[8].equals("") ? null : arrayData[8].trim()));
        newItem.setType((arrayData[9].equals("") ? null : arrayData[9].trim()));
        newItem.setIonicRadius((arrayData[10].equals("") ? null : Float.parseFloat(arrayData[10].trim())));
        newItem.setAtomicRadius((arrayData[11].equals("") ? null : Float.parseFloat(arrayData[11].trim())));
        newItem.setElectronegativity((arrayData[12].equals("") ? null : Float.parseFloat(arrayData[12].trim())));
        newItem.setFirstLonizationPotential((arrayData[13].equals("") ? null : Float.parseFloat(arrayData[13].trim())));
        newItem.setDensity((arrayData[14].equals("") ? null : Double.parseDouble(arrayData[14].trim())));
        newItem.setMeltingPoint((arrayData[15].equals("") ? null : Float.parseFloat(arrayData[15].trim())));
        newItem.setBoilingPoint((arrayData[16].equals("") ? null : Float.parseFloat(arrayData[16].trim())));
        newItem.setIsotopes((arrayData[17].equals("") ? null : Integer.parseInt(arrayData[17].trim())));
        newItem.setDiscoverer((arrayData[18].equals("") ? null : arrayData[18].trim()));
        newItem.setYearOfDiscovery((arrayData[19].equals("") ? null : Integer.parseInt(arrayData[19].trim())));
        newItem.setSpecificHeatCapacity((arrayData[20].equals("") ? null : Float.parseFloat(arrayData[20].trim())));
        newItem.setElectronConfiguration((arrayData[21].equals("") ? null : arrayData[21].trim()));
        newItem.setDisplayRow(Integer.parseInt(arrayData[22].trim()));
        newItem.setDisplayColumn(Integer.parseInt(arrayData[23].trim()));
        
        return newItem;
    }

}
