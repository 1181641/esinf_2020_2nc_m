/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import PL.BST;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author tiagosantos
 */
public class BSTExtend<E extends Comparable<E>> extends BST<E> {

    @Override
    public void insert(E element) {
        super.insert(element); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void remove(E element) {
        super.remove(element); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * Metodo que verifica todas os nodes até encontrar as folhas.
     * @return retorna um mapa do nivel das folhas da arvore.
     */
    public Map<Integer, List<E>> leafByLevel(){
        HashMap<Integer, List<E>> result = new HashMap<Integer, List<E>>();
        
        if(root == null)
            return null;
        
        leafByLevel(root, result, 0);
        return result;
    }
    /**
     * Metodo que verifica todas os nodes até encontrar as folhas.
     * @param node node da arvore.
     * @param result mapa do nivel das folhas da arvore
     * @param level nivel da arvore
     */
    private void leafByLevel(Node<E> node, Map<Integer, List<E>> result, int level){
        List<E> lista = new ArrayList<E>();
        if (result.containsKey(level)) {
            lista = result.get(level);
        }

        if (node.getLeft() == null && node.getRight() == null) {
            lista.add(node.getElement());
            if (lista.size() > 0) 
                result.put(level, lista);
        }else{
            if (node.getLeft() != null) {
                leafByLevel(node.getLeft(), result, level + 1);
            }
            if (node.getRight() != null) {
                leafByLevel(node.getRight(), result, level + 1);
            }
        }
    }
    
    private int findLevel(Node<E> root, E element, int level){
        if (root == null)
            return -1;
        if (root.getElement() == element)
            return level;
        int left = findLevel(root.getLeft(), element, level + 1);
        if (left == -1)
            return findLevel(root.getRight(), element, level + 1);
        return left;
    }
 
    /**
     * metodo que calcula a distancia entre dois elementos.
     * @param node node de inicialização
     * @param eOrig elemento de origem
     * @param eDest elemento de destino
     * @return retorna a distancia entre os dois elementos
     */
    private int findDistance(Node<E> node, E eOrig, E eDest){
        int d1 = findLevel(node, eOrig, 0);
        int d2 = findLevel(node, eDest, 0);
 
        return d1 + d2;
    }
    
    /**
     * Metodo que calcula a distancia entre dois elemntos
     * @param eOrig elemento de origem
     * @param eDest elemento de destino
     * @return retorna a distancia entre os dois elementos
     */
    public int findDistance(E eOrig, E eDest){
        int d1 = findLevel(root, eOrig, 0);
        int d2 = findLevel(root, eDest, 0);
 
        if(d1 == -1 || d2 == -1){
            return 0;
        }else{
            return d1 + d2;
        }
        
    }
    
    /**
     * Método que transforme a árvore obtida alínea anterior numa árvore 
     * binária completa, inserindo nesta possíveis configurações electrónicas únicas.
     * @param resources Lista de electrons a ser inserido caso a arvore não 
     * seja completa.
     * @return retorna verdadeiro caso a arvore seja completa e falso se a 
     * arvore for incompleta.
     */
    public boolean completeBstWithList(List<E> resources){
        return completeBstWithList(root, resources);
    }
    
    /**
     * Método que transforme a árvore obtida alínea anterior numa árvore 
     * binária completa, inserindo nesta possíveis configurações electrónicas únicas.
     * @param node node de inicialização
     * @param resources Lista de electrons a ser inserido caso a arvore não 
     * seja completa.
     * @return retorna verdadeiro caso a arvore seja completa e falso se a 
     * arvore for incompleta.
     */
    private boolean completeBstWithList(Node<E> node, List<E> resources){
        if(node.getLeft() == null && node.getRight() == null){
            return true;
        }else{
            boolean retValue = true;
            List<E> listLeft = new ArrayList<E>();
            List<E> listRight = new ArrayList<E>();
            if (resources != null){
                for(E element : resources){
                    if(node.getElement().compareTo(element) < 0){
                        listRight.add(element);
                    }else if(node.getElement().compareTo(element) > 0){
                        listLeft.add(element);
                    }else{
                        listRight.add(element);
                        listLeft.add(element);
                    }
                }
            }
             
            if(node.getRight()== null){
                if(listRight.size() > 0){
                    node.setRight(new Node<E>(listRight.get(0), null, null));
                }else{
                    retValue = false;
                }
            }else{
                retValue = completeBstWithList(node.getRight(), listRight);
            }
            
            if(!retValue) return false;

            if(node.getLeft() == null){
                if(listLeft.size() > 0){
                    node.setLeft(new Node<E>(listLeft.get(0), null, null));
                }else{
                    retValue = false;
                }
            }else{
                retValue = completeBstWithList(node.getLeft(), listLeft);
            }
            
            return retValue;
        }
    }
}
