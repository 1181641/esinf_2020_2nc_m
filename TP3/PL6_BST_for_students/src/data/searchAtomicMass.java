/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import main.Element;

/**
 *
 * @author hugoc
 */
public class searchAtomicMass {
    
    private List<Element> elementos = new ArrayList<>();
    private Map<String, Map<String, Integer>> sumatorio = new TreeMap<>();

    public List<Element> getElementos() {
        return elementos;
    }

    public void setElementos(List<Element> elementos) {
        this.elementos = elementos;
    }

    public Map<String, Map<String, Integer>> getSumatorio() {
        return sumatorio;
    }

    public void setSumatorio(Map<String, Map<String, Integer>> sumatorio) {
        this.sumatorio = sumatorio;
    }
    
}
