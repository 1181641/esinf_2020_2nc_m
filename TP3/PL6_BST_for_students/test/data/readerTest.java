/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.Iterator;
import main.Element;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author hugoc
 */
public class readerTest {
    
    private reader myReader;
    
    public readerTest() {
    }
    
    @Before
    public void setUp() {
        myReader = new reader();
        
        BSTPeriodic periodicTableAtomicNumber = new BSTPeriodic();
        BSTPeriodic periodicTableElement = new BSTPeriodic();
        BSTPeriodic periodicTableSymbol = new BSTPeriodic();
        BSTPeriodic periodicTableAtomicMass = new BSTPeriodic();
        
        for (int i = 1; i <= 10; i++) {
            Element e = null;
            
            for (int j = 0; j < 4; j++) {
                switch(j) {
                    case 0:
                        e = new Element(true, false, false, false);
                        periodicTableAtomicNumber.insert(insertElementData(e, i));
                        break;
                    case 1:
                        e = new Element(false, true, false, false);
                        periodicTableElement.insert(insertElementData(e, i));
                        break;
                    case 2:
                        e = new Element(false, false, true, false);
                        periodicTableSymbol.insert(insertElementData(e, i));
                        break;
                    case 3:
                        e = new Element(false, false, false, true);
                        periodicTableAtomicMass.insert(insertElementData(e, i));
                        break;
                }
            }   
        }
        
        myReader.setPeriodicTable(periodicTableAtomicNumber);
        myReader.setPeriodicTable_element(periodicTableElement);
        myReader.setPeriodicTable_symbol(periodicTableSymbol);
        myReader.setPeriodicTable_atomicMass(periodicTableAtomicMass);
        
    }
    
    private Element insertElementData(Element e, int i) {
        e.setAtomicNumber(i);
        e.setElement("Element " + i);
        e.setSymbol("S" + i);
        e.setAtomicWeight(Double.parseDouble(String.valueOf(i)));
        e.setAtomicMass(Double.parseDouble(String.valueOf(i)));
        e.setPeriod(i);
        e.setGroup(i);
        e.setPhase("Phase " + i);
        e.setMostStableCrystal(null);
        e.setType("Type " + i);
        e.setIonicRadius(Float.parseFloat(String.valueOf(i)));
        e.setAtomicRadius(Float.parseFloat(String.valueOf(i)));
        e.setElectronegativity(Float.parseFloat(String.valueOf(i)));
        e.setFirstLonizationPotential(Float.parseFloat(String.valueOf(i)));
        e.setDensity(Double.parseDouble(String.valueOf(i)));
        e.setMeltingPoint(Float.parseFloat(String.valueOf(i)));
        e.setBoilingPoint(Float.parseFloat(String.valueOf(i)));
        e.setIsotopes(i);
        e.setDiscoverer("Discoverer " + i);
        e.setYearOfDiscovery(i);
        e.setSpecificHeatCapacity(Float.parseFloat(String.valueOf(i)));
        e.setElectronConfiguration("[HE] 2s" + 1);
        e.setDisplayRow(i);
        e.setDisplayColumn(i);
        
        return e;
    }

    /**
     * Test of getPeriodicTable method, of class reader.
     */
    @Test
    public void testGetPeriodicTable() {        
        System.out.println("getPeriodicTable");
        
        Iterable<Element> i = myReader.getPeriodicTable().inOrder();
        Iterator<Element> it = i.iterator();
        
        int value = 1;
        while(it.hasNext()) {
            Element e = it.next();
            assertTrue("Element number should be <" + value + "> but it was <" + e.getAtomicNumber() + ">", e.getAtomicNumber() == value);
            value++;
        }
    }

    /**
     * Test of getPeriodicTable_element method, of class reader.
     */
    @Test
    public void testGetPeriodicTable_element() {
        System.out.println("getPeriodicTable_element");
        
        Iterable<Element> i = myReader.getPeriodicTable().inOrder();
        Iterator<Element> it = i.iterator();
        
        int value = 1;
        while(it.hasNext()) {
            Element e = it.next();
            assertTrue("Element name should be <Element " + value + "> but it was <" + e.getElement()+ ">", e.getElement().equals("Element " + value));
            value++;
        }
    }

    /**
     * Test of getPeriodicTable_symbol method, of class reader.
     */
    @Test
    public void testGetPeriodicTable_symbol() {
        System.out.println("getPeriodicTable_symbol");
        
        Iterable<Element> i = myReader.getPeriodicTable().inOrder();
        Iterator<Element> it = i.iterator();
        
        int value = 1;
        while(it.hasNext()) {
            Element e = it.next();
            assertTrue("Element symbol should be <S" + value + "> but it was <" + e.getSymbol()+ ">", e.getSymbol().equals("S" + value));
            value++;
        }
    }

    /**
     * Test of getPeriodicTable_atomicMass method, of class reader.
     */
    @Test
    public void testGetPeriodicTable_atomicMass() {
        System.out.println("getPeriodicTable_atomicMass");
        
        Iterable<Element> i = myReader.getPeriodicTable().inOrder();
        Iterator<Element> it = i.iterator();
        
        int value = 1;
        while(it.hasNext()) {
            Element e = it.next();
            assertTrue("Element atomic mass should be <" + value + "> but it was <" + e.getAtomicMass()+ ">", e.getAtomicMass().equals(Double.valueOf(value)));
            value++;
        }
    }

}
