/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import PL.BST;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import main.Element;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author hugoc
 */
public class BSTPeriodicTest {

    private reader myReader;
    private reader myReaderFromFile;

    public BSTPeriodicTest() {
    }

    @Before
    public void setUp() {
        myReader = new reader();
        myReaderFromFile = new reader();
        
        BSTPeriodic periodicTableAtomicNumber = new BSTPeriodic();
        BSTPeriodic periodicTableElement = new BSTPeriodic();
        BSTPeriodic periodicTableSymbol = new BSTPeriodic();
        BSTPeriodic periodicTableAtomicMass = new BSTPeriodic();
        
        for (int i = 1; i <= 10; i++) {
            Element e = null;
            
            for (int j = 0; j < 4; j++) {
                switch(j) {
                    case 0:
                        e = new Element(true, false, false, false);
                        periodicTableAtomicNumber.insert(insertElementData(e, i));
                        break;
                    case 1:
                        e = new Element(false, true, false, false);
                        periodicTableElement.insert(insertElementData(e, i));
                        break;
                    case 2:
                        e = new Element(false, false, true, false);
                        periodicTableSymbol.insert(insertElementData(e, i));
                        break;
                    case 3:
                        e = new Element(false, false, false, true);
                        periodicTableAtomicMass.insert(insertElementData(e, i));
                        break;
                }
            }   
        }
        
        myReader.setPeriodicTable(periodicTableAtomicNumber);
        myReader.setPeriodicTable_element(periodicTableElement);
        myReader.setPeriodicTable_symbol(periodicTableSymbol);
        myReader.setPeriodicTable_atomicMass(periodicTableAtomicMass);
    }
    
    
    private Element insertElementData(Element e, int i) {
        e.setAtomicNumber(i);
        e.setElement("Element " + i);
        e.setSymbol("S" + i);
        e.setAtomicWeight(Double.parseDouble(String.valueOf(i)));
        e.setAtomicMass(Double.parseDouble(String.valueOf(i)));
        e.setPeriod(i);
        e.setGroup(i);
        e.setPhase("Phase " + i);
        e.setMostStableCrystal(null);
        e.setType("Type " + i);
        e.setIonicRadius(Float.parseFloat(String.valueOf(i)));
        e.setAtomicRadius(Float.parseFloat(String.valueOf(i)));
        e.setElectronegativity(Float.parseFloat(String.valueOf(i)));
        e.setFirstLonizationPotential(Float.parseFloat(String.valueOf(i)));
        e.setDensity(Double.parseDouble(String.valueOf(i)));
        e.setMeltingPoint(Float.parseFloat(String.valueOf(i)));
        e.setBoilingPoint(Float.parseFloat(String.valueOf(i)));
        e.setIsotopes(i);
        e.setDiscoverer("Discoverer " + i);
        e.setYearOfDiscovery(i);
        e.setSpecificHeatCapacity(Float.parseFloat(String.valueOf(i)));
        e.setElectronConfiguration("[HE] 2s" + (i % 2 == 0 ? 1: 2) + " 3f" + i);
        e.setDisplayRow(i);
        e.setDisplayColumn(i);
        
        return e;
    }

    /**
     * Test of find method, of class BSTPeriodic.
     */
    @Test
    public void testFind() {
        System.out.println("find");

        Element retValue;
        String expValue_STR;

        // NULL VALUE
        retValue = myReaderFromFile.getPeriodicTable().find(0);
        assertTrue("Find Atomic Number - Return value should be <null>.", retValue == null);
        retValue = myReaderFromFile.getPeriodicTable_element().find("");
        assertTrue("Find Element - Return value should be <null>.", retValue == null);
        retValue = myReaderFromFile.getPeriodicTable_symbol().find("");
        assertTrue("Find Symbol - Return value should be <null>.", retValue == null);
        retValue = myReaderFromFile.getPeriodicTable_atomicMass().find(0D);
        assertTrue("Find Atomic Mass - Return value should be <null>.", retValue == null);

        // ELEMENT -> Boron
        expValue_STR = "Boron";
        retValue = myReaderFromFile.getPeriodicTable().find(5);
        assertTrue("Find Element - Element should be <" + expValue_STR + "> but it was <" + retValue.getElement() + ">",
                retValue.getElement().equals(expValue_STR));
        retValue = myReaderFromFile.getPeriodicTable_element().find("Boron");
        expValue_STR = "Boron";
        assertTrue("Find Element - Element should be <" + expValue_STR + "> but it was <" + retValue.getElement() + ">",
                retValue.getElement().equals(expValue_STR));
        retValue = myReaderFromFile.getPeriodicTable_symbol().find("B");
        assertTrue("Find Symbol - Element should be <" + expValue_STR + "> but it was <" + retValue.getElement() + ">",
                retValue.getElement().equals(expValue_STR));
        retValue = myReaderFromFile.getPeriodicTable_atomicMass().find(10.81D);
        assertTrue("Find Atomic Mass - Element should be <" + expValue_STR + "> but it was <" + retValue.getElement() + ">",
                retValue.getElement().equals(expValue_STR));

        // ELEMENT -> Tin
        expValue_STR = "Tin";
        retValue = myReaderFromFile.getPeriodicTable().find(50);
        assertTrue("Find Element - Element should be <" + expValue_STR + "> but it was <" + retValue.getElement() + ">",
                retValue.getElement().equals(expValue_STR));
        retValue = myReaderFromFile.getPeriodicTable_element().find("Tin");
        assertTrue("Find Element - Element should be <" + expValue_STR + "> but it was <" + retValue.getElement() + ">",
                retValue.getElement().equals(expValue_STR));
        retValue = myReaderFromFile.getPeriodicTable_symbol().find("Sn");
        assertTrue("Find Symbol - Element should be <" + expValue_STR + "> but it was <" + retValue.getElement() + ">",
                retValue.getElement().equals(expValue_STR));
        retValue = myReaderFromFile.getPeriodicTable_atomicMass().find(118.71D);
        assertTrue("Find Atomic Mass - Element should be <" + expValue_STR + "> but it was <" + retValue.getElement() + ">",
                retValue.getElement().equals(expValue_STR));

        
        // ELEMENT -> Argon
        expValue_STR = "Argon";
        retValue = myReaderFromFile.getPeriodicTable().find(18);
        assertTrue("Find Element - Element should be <" + expValue_STR + "> but it was <" + retValue.getElement() + ">",
                retValue.getElement().equals(expValue_STR));
        retValue = myReaderFromFile.getPeriodicTable_element().find("Argon");
        assertTrue("Find Element - Element should be <" + expValue_STR + "> but it was <" + retValue.getElement() + ">",
                retValue.getElement().equals(expValue_STR));
        retValue = myReaderFromFile.getPeriodicTable_symbol().find("Ar");
        assertTrue("Find Symbol - Element should be <" + expValue_STR + "> but it was <" + retValue.getElement() + ">",
                retValue.getElement().equals(expValue_STR));
        retValue = myReaderFromFile.getPeriodicTable_atomicMass().find(39.9);
        assertTrue("Find Atomic Mass - Element should be <" + expValue_STR + "> but it was <" + retValue.getElement() + ">",
                retValue.getElement().equals(expValue_STR));

    }

    /**
     * Test of getAtomicMassBetween method, of class BSTPeriodic.
     */
    @Test
    public void testGetAtomicMassBetween() {
        System.out.println("getAtomicMassBetween");
        searchAtomicMass result;
        double min = 0.0;
        double max = 0.0;
        result = myReaderFromFile.getPeriodicTable_atomicMass().getAtomicMassBetween(min, max);
        assertTrue("Size of elements should be <0> but it was <" + result.getElementos().size() + ">", result.getElementos().size() == 0);
        
        min = 20.0;
        max = 40.0;
        result = myReaderFromFile.getPeriodicTable_atomicMass().getAtomicMassBetween(min, max);
        assertTrue("Size of elements should be <10> but it was <" + result.getElementos().size() + ">", result.getElementos().size() == 10);
        String[] arrElements = {"Silicon", "Magnesium", "Phosphorus", "Sodium", "Potassium", "Sulfur", "Neon", "Argon", "Chlorine", "Aluminum"};

        Iterator<Element> it = result.getElementos().iterator();
        int index = 0;
        while (it.hasNext()) {
            Element e = it.next();
            assertTrue("Value of index " + index + " should be <" + arrElements[index] + "> but it was <" + e.getElement() + ">",
                    e.getElement().equals(arrElements[index]));
            index++;
        }

        String[] arrTypes = {"Alkali Metal", "Alkaline Earth Metal", "Halogen", "Metal", "Metalloid", "Noble Gas", "Nonmetal"};
        int[] arrTotals = {2, 1, 1, 1, 1, 2, 2};
        for (int i = 0; i < arrTypes.length; i++) {
            assertTrue("Total of <" + arrTypes[i] + "> should be <" + arrTotals[i] + "> but it was <" + result.getSumatorio().get(arrTypes[i]).get("total") + ">",
                    result.getSumatorio().get(arrTypes[i]).get("total") == arrTotals[i]);
        }
    }

    /**
     * Test of ex2a method, of class BSTPeriodic.
     */
    @Test
    public void testEx2a() {
        System.out.println("ex2a");
        int limit = 1;
        TreeMap<Integer, List<String>> result = myReader.getPeriodicTable().ex2a(20);
        assertTrue("Result should be empty.", result.isEmpty());
        
        result = myReader.getPeriodicTable().ex2a(10);
        List<String> lstResult = result.get(5);
        assertTrue("Result list should be empty.", lstResult == null);
        
        result = myReader.getPeriodicTable().ex2a(limit);
        lstResult = result.get(10);
        List<String> lstExpected = new ArrayList<>();
        lstExpected.add("[HE]");
        assertTrue("Result should have values.", !lstResult.isEmpty());
        assertTrue("Value should be <" + lstExpected.get(0) + "> but it was <" + lstResult.get(0) + ">", 
                lstResult.get(0).equals(lstExpected.get(0)));
        
        lstResult = result.get(5);
        lstExpected = new ArrayList<>();
        lstExpected.add("[HE] 2s1");       
        lstExpected.add("[HE] 2s2");
        assertTrue("Value should be <" + lstExpected.get(0) + "> but it was <" + lstResult.get(0) + ">", 
                lstResult.get(0).equals(lstExpected.get(0)));
        assertTrue("Value should be <" + lstExpected.get(1) + "> but it was <" + lstResult.get(1) + ">", 
                lstResult.get(1).equals(lstExpected.get(1)));
        
    }

    /**
     * Test of ex2b method, of class BSTPeriodic.
     */
    @Test
    public void testEx2b() {
        System.out.println("ex2b");
        TreeMap<Integer, List<String>> result_2 = myReader.getPeriodicTable().ex2a(1);
        BST result = myReader.getPeriodicTable().ex2b(result_2);
        Iterable i = result.inOrder();
        Iterator it = i.iterator();
        
        String[] arr = {"[HE]", "[HE] 2s1", "[HE] 2s2"};        
        int index = 0;
        while (it.hasNext()) {
            String x = (String)it.next();
            assertTrue("Value should be <> but it was <>", x.equals(arr[index]));
            index++;
        }
        
    }

    /**
     * Test of ex2c method, of class BSTPeriodic.
     */
    @Test
    public void testEx2c() {
        System.out.println("ex2c");

        Map<Integer, List<String>> result = myReader.getPeriodicTable().ex2c(null);
        assertTrue("Longest Distance - Returned object should be null.", result == null);

        BSTExtend bstExample = new BSTExtend();
        result = myReader.getPeriodicTable().ex2c(bstExample);
        assertTrue("Longest Distance - Returned object should be null.", result == null);
        
        TreeMap<Integer, List<String>> mapResult = new TreeMap<Integer, List<String>>();
        String[] array = {"[Xe] 4f1","[Kr] 5s1","[Xe] 4f9","[Ar] 3d10 4s2 4p2","[Xe] 4f1 5d1","[Ne] 3s2 3p2","[Xe] 4f14 5d10 6s1","[Xe] 4f14 5d4 6s2","[Ar] 3d10 4s2 4p6","[Rn] 6d1","[Xe] 4f11 6s2","[Xe] 4f5 6s2","[Rn] 5f4 6d1 7s2","[Ar] 3d3","[He] 2s1","[Ar] 3d2 4s2","[Xe] 4f10","1s1","[He] 2s2 2p3","[Rn] 5f2","[He] 2s2 2p4","[Rn] 7s1","[Xe] 4f9 6s2","[Ar] 3d6","[Ar] 3d1 4s2","[Ar] 3d7","[Xe] 4f14 5d6 6s2","[Ne] 3s2 3p5","[Rn] 5f2 6d1 7s2","[Xe] 4f12","[Xe] 4f4","[Xe] 4f14 5d10 6s2 6p5","[Kr] 4d10 5s2 5p2","[Xe] 4f14 5d5","[Kr] 4d4 5s1","[Xe] 4f14 5d5 6s2","[Xe] 4f14 5d10 6s2 6p2","[Kr] 4d8","[Xe] 4f14 5d7 6s2","[Ar] 3d10 4s1","[Ne] 3s1","[Rn] 5f7 7s2","[Xe] 4f4 6s2","[Rn] 5f4 6d1","[Xe] 4f14 5d10 6s2 6p1","[Xe] 4f11","[He] 2s2 2p2","[Ne] 3s2 3p1","[Xe] 4f14 5d7","[Xe] 4f14 5d3","[Ar] 3d10 4s2 4p4","[Xe] 4f14 5d10 6s2 6p3","[Xe] 6s1","[Xe] 4f14 5d9","[Ar] 3d10 4s2 4p5","[Ar] 3d10 4s2 4p1","[Kr] 5s2","[Rn] 5f2 6d1","[Rn] 6d2 7s2","[Xe] 4f14 5d10 6s2 6p6","[Xe] 4f14 5d3 6s2","[Xe] 6s2","[Xe] 4f3 6s2","[Kr] 4d2 5s2","[Rn] 5f3 6d1 7s2","[Ne] 3s2 3p3","[Ar] 4s2","[Xe] 4f13 6s2","[Kr] 4d8 5s1","[Ar] 3d5 4s1","[Rn] 5f4","[Xe] 4f14 5d6","[Kr] 4d10 5s2 5p3","[Xe] 4f7 5d1","[Ar] 3d10 4s2 4p3","[Kr] 4d10 5s2 5p4","[Xe] 4f14 5d1 6s2","[Rn] 5f3","[Kr] 4d10 5s2 5p1","1s2","[Ne] 3s2 3p4","[Xe] 4f7 5d1 6s2","[Xe] 4f14 5d2 6s2","[He] 2s2 2p5","[Xe] 5d1","[Kr] 4d10 5s1","[Ar] 3d3 4s2","[Rn] 6d1 7s2","[Rn] 6d2","[Xe] 4f14 5d2","[Xe] 4f14 5d10 6s2 6p4","[Kr] 4d1","[Xe] 4f10 6s2","[Ar] 3d2","[Xe] 4f1 5d1 6s2","[Xe] 4f14 5d9 6s1","[Ar] 3d8 4s2","[Ar] 3d5 4s2","[Kr] 4d1 5s2","[Xe] 4f12 6s2","[Ar] 3d6 4s2","[He] 2s2 2p6","[Kr] 4d5 5s2","[Xe] 4f14 5d1","[Xe] 4f5","[Kr] 4d7 5s1","[He] 2s2 2p1","[Xe] 4f3","[Kr] 4d10 5s2 5p5","[Kr] 4d2","[Ar] 3d7 4s2","[Ar] 3d8","[Ar] 3d1","[Kr] 4d4","[Xe] 5d1 6s2","[Rn] 5f6 7s2","[Kr] 4d10 5s2 5p6","[Xe] 4f7 6s2","[Rn] 7s2","[Ne] 3s2 3p6","[Rn] 5f3 6d1","[Ar] 4s1","[Xe] 4f13","[Rn] 5f6","[Xe] 4f14 5d4","[Kr] 4d5 5s1","[Xe] 4f6 6s2","[Xe] 4f6","[Kr] 4d7","[Rn] 5f7","[Xe] 4f14 6s2"};
        List<String>  listArray = Arrays.asList(array);
        mapResult.put(1, listArray);
        bstExample = myReader.getPeriodicTable().ex2b(mapResult);
        
        result = myReader.getPeriodicTable().ex2c(bstExample);
        assertTrue("Longest Distance - Returned size should be 1.", result.size() == 1);
        assertTrue("Longest Distance - The longest distance should be 28.", result.containsKey(28));
        List<String> listResult = result.get(28);
        assertTrue("Longest Distance - The size of the number of nodes with the longest distance should be 4.", listResult.size() == 4);
        assertTrue("Longest Distance - The nodes shoulb be <[Kr] 4d10 5s2 5p6> and <[He] 2s2 2p6>.", listResult.get(0).compareTo("D([Kr] 4d10 5s2 5p6,[He] 2s2 2p6)") == 0);
        assertTrue("Longest Distance - The nodes shoulb be <[Kr] 4d10 5s2 5p6> and <[Xe] 4f14 6s2>.", listResult.get(3).compareTo("D([Kr] 4d10 5s2 5p6,[Xe] 4f14 6s2)") == 0);
        
        
        mapResult = new TreeMap<Integer, List<String>>();
        array = new String[]{"[Xe] 4f1","[Kr] 5s1","[Xe] 4f9","[Ar] 3d10 4s2 4p2","[Xe] 4f1 5d1","[Ne] 3s2 3p2","[Xe] 4f14 5d10 6s1","[Xe] 4f14 5d4 6s2","[Ar] 3d10 4s2 4p6","[Rn] 6d1","[Xe] 4f11 6s2","[Xe] 4f5 6s2","[Rn] 5f4 6d1 7s2","[Ar] 3d3","[He] 2s1","[Ar] 3d2 4s2","[Xe] 4f10","1s1","[He] 2s2 2p3","[Rn] 5f2","[He] 2s2 2p4","[Rn] 7s1","[Xe] 4f9 6s2","[Ar] 3d6","[Ar] 3d1 4s2","[Ar] 3d7","[Xe] 4f14 5d6 6s2","[Ne] 3s2 3p5","[Rn] 5f2 6d1 7s2","[Xe] 4f12","[Xe] 4f4","[Xe] 4f14 5d10 6s2 6p5","[Kr] 4d10 5s2 5p2","[Xe] 4f14 5d5","[Kr] 4d4 5s1","[Xe] 4f14 5d5 6s2","[Xe] 4f14 5d10 6s2 6p2","[Kr] 4d8","[Xe] 4f14 5d7 6s2","[Ar] 3d10 4s1","[Ne] 3s1","[Rn] 5f7 7s2","[Xe] 4f4 6s2","[Rn] 5f4 6d1","[Xe] 4f14 5d10 6s2 6p1","[Xe] 4f11","[He] 2s2 2p2","[Ne] 3s2 3p1","[Xe] 4f14 5d7","[Xe] 4f14 5d3"};
        listArray = Arrays.asList(array);
        mapResult.put(1, listArray);
        bstExample = myReader.getPeriodicTable().ex2b(mapResult);
        
        result = myReader.getPeriodicTable().ex2c(bstExample);
        assertTrue("Longest Distance - Returned size should be 1.", result.size() == 1);
        assertTrue("Longest Distance - The longest distance should be 19.", result.containsKey(19));
        listResult = result.get(19);
        assertTrue("Longest Distance - The size of the number of nodes with the longest distance should be 1.", listResult.size() == 1);
        assertTrue("Longest Distance - The nodes shoulb be <[Kr] 4d8> and <[Xe] 4f14 5d7>.", listResult.get(0).compareTo("D([Kr] 4d8,[Xe] 4f14 5d7)") == 0);
        
        
    }
    

}
