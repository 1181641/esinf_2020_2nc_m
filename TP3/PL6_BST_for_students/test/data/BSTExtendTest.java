/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tiagosantos
 */
public class BSTExtendTest {
    
    private reader myReader;
    private BSTExtend bstResult;
    private TreeMap<Integer, List<String>> result;
    
    public BSTExtendTest() {
        myReader = new reader();
        
        result = new TreeMap<Integer, List<String>>();
        String[] array = {"[Xe]"};
        List<String> listArray = Arrays.asList(array);
        result.put(32, listArray);
        array = new String[] {"[Ar]", "[Kr]"};
        listArray = Arrays.asList(array);
        result.put(18, listArray);
        array = new String[] {"[Xe] 4f14"};
        listArray = Arrays.asList(array);
        result.put(17, listArray);
        array = new String[] {"[Kr] 4d10", "[Rn]"};
        listArray = Arrays.asList(array);
        result.put(9, listArray);
        array = new String[] {"[Ar] 3d10", "[He]", "[Ne]", "[Xe] 4f14 5d10"};
        listArray = Arrays.asList(array);
        result.put(8, listArray);
        array = new String[] {"[Ar] 3d10 4s2", "[He] 2s2", "[Kr] 4d10 5s2", "[Ne] 3s2", "[Xe] 4f14 5d10 6s2"};
        listArray = Arrays.asList(array);
        result.put(7, listArray);
        array = new String[] {"[Ar] 3d5", "[Kr] 4d5", "[Xe] 4f7"};
        listArray = Arrays.asList(array);
        result.put(2, listArray);
        array = new String[] {"1s1", "1s2", "[Ar] 3d1", "[Ar] 3d1 4s2", "[Ar] 3d10 4s1", "[Ar] 3d10 4s2 4p1", "[Ar] 3d10 4s2 4p2", "[Ar] 3d10 4s2 4p3", "[Ar] 3d10 4s2 4p4", "[Ar] 3d10 4s2 4p5", "[Ar] 3d10 4s2 4p6", "[Ar] 3d2", "[Ar] 3d2 4s2", "[Ar] 3d3", "[Ar] 3d3 4s2", "[Ar] 3d5 4s1", "[Ar] 3d5 4s2", "[Ar] 3d6", "[Ar] 3d6 4s2", "[Ar] 3d7", "[Ar] 3d7 4s2", "[Ar] 3d8", "[Ar] 3d8 4s2", "[Ar] 4s1", "[Ar] 4s2", "[He] 2s1", "[He] 2s2 2p1", "[He] 2s2 2p2", "[He] 2s2 2p3", "[He] 2s2 2p4", "[He] 2s2 2p5", "[He] 2s2 2p6", "[Kr] 4d1", "[Kr] 4d1 5s2", "[Kr] 4d10 5s1", "[Kr] 4d10 5s2 5p1", "[Kr] 4d10 5s2 5p2", "[Kr] 4d10 5s2 5p3", "[Kr] 4d10 5s2 5p4", "[Kr] 4d10 5s2 5p5", "[Kr] 4d10 5s2 5p6", "[Kr] 4d2", "[Kr] 4d2 5s2", "[Kr] 4d4", "[Kr] 4d4 5s1", "[Kr] 4d5 5s1", "[Kr] 4d5 5s2", "[Kr] 4d7", "[Kr] 4d7 5s1", "[Kr] 4d8", "[Kr] 4d8 5s1", "[Kr] 5s1", "[Kr] 5s2", "[Ne] 3s1", "[Ne] 3s2 3p1", "[Ne] 3s2 3p2", "[Ne] 3s2 3p3", "[Ne] 3s2 3p4", "[Ne] 3s2 3p5", "[Ne] 3s2 3p6", "[Rn] 5f2", "[Rn] 5f2 6d1", "[Rn] 5f2 6d1 7s2", "[Rn] 5f3", "[Rn] 5f3 6d1", "[Rn] 5f3 6d1 7s2", "[Rn] 5f4", "[Rn] 5f4 6d1", "[Rn] 5f4 6d1 7s2", "[Rn] 5f6", "[Rn] 5f6 7s2", "[Rn] 5f7", "[Rn] 5f7 7s2", "[Rn] 6d1", "[Rn] 6d1 7s2", "[Rn] 6d2", "[Rn] 6d2 7s2", "[Rn] 7s1", "[Rn] 7s2", "[Xe] 4f1", "[Xe] 4f1 5d1", "[Xe] 4f1 5d1 6s2", "[Xe] 4f10", "[Xe] 4f10 6s2", "[Xe] 4f11", "[Xe] 4f11 6s2", "[Xe] 4f12", "[Xe] 4f12 6s2", "[Xe] 4f13", "[Xe] 4f13 6s2", "[Xe] 4f14 5d1", "[Xe] 4f14 5d1 6s2", "[Xe] 4f14 5d10 6s1", "[Xe] 4f14 5d10 6s2 6p1", "[Xe] 4f14 5d10 6s2 6p2", "[Xe] 4f14 5d10 6s2 6p3", "[Xe] 4f14 5d10 6s2 6p4", "[Xe] 4f14 5d10 6s2 6p5", "[Xe] 4f14 5d10 6s2 6p6", "[Xe] 4f14 5d2", "[Xe] 4f14 5d2 6s2", "[Xe] 4f14 5d3", "[Xe] 4f14 5d3 6s2", "[Xe] 4f14 5d4", "[Xe] 4f14 5d4 6s2", "[Xe] 4f14 5d5", "[Xe] 4f14 5d5 6s2", "[Xe] 4f14 5d6", "[Xe] 4f14 5d6 6s2", "[Xe] 4f14 5d7", "[Xe] 4f14 5d7 6s2", "[Xe] 4f14 5d9", "[Xe] 4f14 5d9 6s1", "[Xe] 4f14 6s2", "[Xe] 4f3", "[Xe] 4f3 6s2", "[Xe] 4f4", "[Xe] 4f4 6s2", "[Xe] 4f5", "[Xe] 4f5 6s2", "[Xe] 4f6", "[Xe] 4f6 6s2", "[Xe] 4f7 5d1", "[Xe] 4f7 5d1 6s2", "[Xe] 4f7 6s2", "[Xe] 4f9", "[Xe] 4f9 6s2", "[Xe] 5d1", "[Xe] 5d1 6s2", "[Xe] 6s1", "[Xe] 6s2"};
        listArray = Arrays.asList(array);
        result.put(1, listArray);
        
        TreeMap<Integer, List<String>> result2 = (TreeMap<Integer, List<String>>)result.clone();
        result2.remove(1);
        
        bstResult = myReader.getPeriodicTable().ex2b(result2);
        
    }
    
    @Before
    public void setUp() {
    }

    /**
     * Test of leafByLevel method, of class BSTExtend.
     */
    @Test
    public void testLeafByLevel() {
        System.out.println("leafByLevel");
        
        BSTExtend bstNull = new BSTExtend();
        Map<Integer, List<String>> result = bstNull.leafByLevel();
        assertTrue("Leaf By Level - Returned value should be <null>.", result == null);
        
        result = bstResult.leafByLevel();
        assertTrue("Leaf By Level - Returned size should be 4.", result.size() == 4);
        List<String> resultList = result.get(3);
        assertTrue("Leaf By Level - Key should be <3> and the size should be 2.", resultList.size() == 2);
        resultList = result.get(4);
        assertTrue("Leaf By Level - Key should be <4> and the size should be 1.", resultList.size() == 1);
        resultList = result.get(5);
        assertTrue("Leaf By Level - Key should be <5> and the size should be 1.", resultList.size() == 1);
        resultList = result.get(8);
        assertTrue("Leaf By Level - Key should be <8> and the size should be 1.", resultList.size() == 1);
        
        bstResult.remove("[He]");
        bstResult.remove("[Ne]");
        bstResult.remove("[Kr]");
        bstResult.remove("[Xe]");
        result = bstResult.leafByLevel();
        assertTrue("Leaf By Level - Returned size should be 3.", result.size() == 3);
        resultList = result.get(3);
        assertTrue("Leaf By Level - Key should be <3> and the size should be 1.", resultList.size() == 1);
        resultList = result.get(4);
        assertTrue("Leaf By Level - Key should be <4> and the size should be 1.", resultList.size() == 1);
        resultList = result.get(7);
        assertTrue("Leaf By Level - Key should be <7> and the size should be 1.", resultList.size() == 1);
        
    }

    /**
     * Test of findDistance method, of class BSTExtend.
     */
    @Test
    public void testFindDistance() {
        System.out.println("findDistance");

        assertTrue("Find Distance - Distance between <[Nada]> and <[Kr]> should be 0.", bstResult.findDistance("[Nada]", "[Kr]") == 0);
        assertTrue("Find Distance - Distance between <[He]> and <[Kr]> should be 8.", bstResult.findDistance("[He]", "[Kr]") == 8);
        assertTrue("Find Distance - Distance between <[Ar] 3d10 4s2> and <[Kr]> should be 6.", bstResult.findDistance("[Ar] 3d10 4s2", "[Kr]") == 6);
        assertTrue("Find Distance - Distance between <[Ar] 3d10 4s2> and <[Xe]> should be 9.", bstResult.findDistance("[Ar] 3d10 4s2", "[Xe]") == 9);
        assertTrue("Find Distance - Distance between <[Ar] 3d10 4s2> and <[Xe] 4f7> should be 3.", bstResult.findDistance("[Ar] 3d10 4s2", "[Xe] 4f7") == 3);
        assertTrue("Find Distance - Distance between <[Ar] 3d10 4s2> and <[Ar] 3d10> should be 3.", bstResult.findDistance("[Ar] 3d10 4s2", "[Ar] 3d10") == 3);
        
    }

    /**
     * Test of completeBstWithList method, of class BSTExtend.
     */
    @Test
    public void testCompleteBstWithList() {
        System.out.println("completeBstWithList");

        assertFalse("Complete BST with List - BST shoulb be incomplete.", bstResult.completeBstWithList(null));
        assertTrue("Complete BST with List - BST shoulb be complete.", bstResult.completeBstWithList(result.get(1)));
        
    }
    
}
