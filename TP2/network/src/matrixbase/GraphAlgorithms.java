
package matrixbase;

import graphbase.Edge;
import graphbase.Graph;
import graphbase.Vertex;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * Implementation of graph algorithms for a (undirected) graph structure 
 * Considering generic vertex V and edge E types
 * 
 * Works on AdjancyMatrixGraph objects
 * 
 * @author DEI-ESINF
 * 
 */
public class GraphAlgorithms {

    
    /**
     * Performs depth-first search of the graph starting at vertex.
     * Calls package recursive version of the method.
     *
     * @param graph Graph object
     * @param vertex Vertex of graph that will be the source of the search
     * @return queue of vertices found by search (including vertex), null if vertex does not exist
     *
     */
    public static <V,E> LinkedList<V> BFS(AdjacencyMatrixGraph<V,E> graph, V vertex) {

       int index = graph.toIndex(vertex);
        if (index == -1)
            return null;

        LinkedList<V> resultQueue = new LinkedList<V>();
        LinkedList<Integer> auxQueue = new LinkedList<Integer>();

        resultQueue.add(graph.vertices.get(index));
        auxQueue.add(index);

        while(!auxQueue.isEmpty()){
            index=auxQueue.remove(); 
            for (int i = 0 ; i < graph.numVertices ; i++){
                if (graph.edgeMatrix[index][i] != null){
                    if (!resultQueue.contains(graph.vertices.get(i))){
                        resultQueue.add(graph.vertices.get(i));
                        auxQueue.add(i);
                    }
                }
            }
        }
        return resultQueue;	
    }

    /**
     * Performs depth-first search of the graph starting at vertex.
     * Calls package recursive version of the method.
     * @param graph Graph object
     * @param vertex Vertex of graph that will be the source of the search
     * @return queue of vertices found by search (empty if none), null if vertex does not exist
     */
    public static <V,E> LinkedList<V> DFS(AdjacencyMatrixGraph<V,E> graph, V vertex) {
        
        if(!graph.checkVertex(vertex))
            return null;
        
        LinkedList<V> verticesQueue = new LinkedList<V>();
        verticesQueue.add(vertex);
        
        for(V vAdj: graph.directConnections(vertex)){
            if (!verticesQueue.contains(vAdj)){
                DFS(graph, graph.toIndex(vAdj), verticesQueue);
            }
        }
        
        return verticesQueue;
    }

    /**
     * Actual depth-first search of the graph starting at vertex.
     * The method adds discovered vertices (including vertex) to the queue of vertices
     * @param graph Graph object
     * @param index Index of vertex of graph that will be the source of the search
     * @param verticesQueue queue of vertices found by search
     *
     */
    static <V,E> void DFS(AdjacencyMatrixGraph<V,E> graph, int index, LinkedList<V> verticesQueue) {
        
        V vOrig = graph.vertices.get(index);
        verticesQueue.add(vOrig);
                
        for(V vAdj : graph.directConnections(vOrig)){
            if (!verticesQueue.contains(vAdj)){
                DFS(graph, graph.toIndex(vAdj), verticesQueue);
            }
        }
    }
    

    /**
     * Transforms a graph into its transitive closure 
     * uses the Floyd-Warshall algorithm
     * 
     * @param graph Graph object
     * @param dummyEdge object to insert in the newly created edges
     * @return the new graph 
     */
    public static <V, E> AdjacencyMatrixGraph<V, E> transitiveClosure(AdjacencyMatrixGraph<V, E> graph, E dummyEdge){
        throw new UnsupportedOperationException("Not supported yet.");
    }

    // Function to construct and print MST for a graph represented 
    // using adjacency matrix representation 
    public static <V, E> LinkedList<String> MST(AdjacencyMatrixGraph<V, E> graph) {
        
        LinkedList<String> retValue = new LinkedList<>();
        LinkedList<V> vertices = new LinkedList<V>();
        boolean[] visited = new boolean[graph.numVertices];
        int index = 0;
        
        visited[index] = true;
        vertices.push(graph.vertices.get(index));
        
        while(!vertices.isEmpty()){
            index = graph.toIndex(vertices.peek());
            
            int indexNotVisited = -1;
            for(int i = 0; i < graph.numVertices; i++){
                if(graph.getEdge(graph.vertices.get(index), graph.vertices.get(i)) != null && visited[i] == false){
                    indexNotVisited = i;
                    break;
                }
            }
            
            if(indexNotVisited == -1){
                vertices.pop();
            }else{
                visited[indexNotVisited] = true;
                //System.out.println(graph.vertices.get(index) + "-" + graph.vertices.get(indexNotVisited));
                retValue.add(graph.vertices.get(index) + "-" + graph.vertices.get(indexNotVisited));
                vertices.push(graph.vertices.get(indexNotVisited));
            }
        }
        
        return retValue; 
    } 
}
