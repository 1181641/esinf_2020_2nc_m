/*
* A collection of graph algorithms.
 */
package graphbase;

import java.awt.geom.QuadCurve2D;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.TreeMap;

/**
 *
 * @author DEI-ESINF
 */
public class GraphAlgorithms {

    /**
     * Performs breadth-first search of a Graph starting in a Vertex
     *
     * @param g Graph instance
     * @param vInf information of the Vertex that will be the source of the
     * search
     * @return qbfs a queue with the vertices of breadth-first search
     */
    public static <V, E> LinkedList<V> BreadthFirstSearch(Graph<V, E> g, V vert) {
        if (!g.validVertex(vert)) {
            return null;
        }

        LinkedList<V> qbfs = new LinkedList<>();
        LinkedList<V> qaux = new LinkedList<>();

        qbfs.add(vert);
        qaux.add(vert);

        while (!qaux.isEmpty()) {
            for (V vAdj : g.adjVertices(qaux.getFirst())) {
                if (!qbfs.contains(vAdj)) {
                    qbfs.add(vAdj);
                    qaux.add(vAdj);
                }
            }
            
            //REMOVE vert from quax
            qaux.removeFirst();
        }

        return qbfs;
    }

    /**
     * Performs depth-first search starting in a Vertex
     *
     * @param g Graph instance
     * @param vOrig Vertex of graph g that will be the source of the search
     * @param visited set of discovered vertices
     * @param qdfs queue with vertices of depth-first search
     */
    private static <V, E> void DepthFirstSearch(Graph<V, E> g, V vOrig, LinkedList<V> qdfs) {
        if (!qdfs.contains(vOrig)) {
            qdfs.add(vOrig);
        }

        for (V vAdj : g.adjVertices(vOrig)) {
            if (!qdfs.contains(vAdj)) {
                DepthFirstSearch(g, vAdj, qdfs);
            }
        }
    }

    /**
     * @param g Graph instance
     * @param vInf information of the Vertex that will be the source of the
     * search
     * @return qdfs a queue with the vertices of depth-first search
     */
    public static <V, E> LinkedList<V> DepthFirstSearch(Graph<V, E> g, V vert) {
        if (!g.validVertex(vert)) {
            return null;
        }
        
        LinkedList<V> qdfs = new LinkedList<>();
        qdfs.add(vert);
        
        DepthFirstSearch(g, vert, qdfs);
        
        System.out.println(qdfs);
        return qdfs;
    }

    /**
     * Returns all paths from vOrig to vDest
     *
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param vDest Vertex that will be the end of the path
     * @param visited set of discovered vertices
     * @param path stack with vertices of the current path (the path is in
     * reverse order)
     * @param paths ArrayList with all the paths (in correct order)
     */
    private static <V, E> void allPaths(Graph<V, E> g, V vOrig, V vDest,
            LinkedList<V> path, ArrayList<LinkedList<V>> paths) {
        if (vOrig.equals(vDest)) {
            System.out.println(path);
            paths.add((LinkedList<V>) path.clone());
            return; 
        } 
  
        for (V vAdj : g.adjVertices(vOrig)) {
            if (!path.contains(vAdj)) {
                path.add(vAdj);
                allPaths(g, vAdj, vDest, path, paths);
                path.remove(vAdj);
            }
        }
    }

    /**
     * @param g Graph instance
     * @param vOrig information of the Vertex origin
     * @param vDest information of the Vertex destination
     * @return paths ArrayList with all paths from voInf to vdInf
     */
    public static <V, E> ArrayList<LinkedList<V>> allPaths(Graph<V, E> g, V vOrig, V vDest) {
        if (!g.validVertex(vOrig) || !g.validVertex(vDest)) {
            return null;
        }
        
        LinkedList<V> path = new LinkedList<>();
        ArrayList<LinkedList<V>> paths = new ArrayList<>();
        
        path.add(vOrig);
        allPaths(g, vOrig, vDest, path, paths);
        
        return paths;
    }
    
    /**
     * Returns all paths from vOrig to vDest
     *
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param vDest Vertex that will be the end of the path
     * @param visited set of discovered vertices
     * @param path stack with vertices of the current path (the path is in
     * reverse order)
     * @param paths ArrayList with all the paths (in correct order)
     */
    private static <V, E> void allPathsWithMustVisit(Graph<V, E> g, V vOrig, V vDest,
            LinkedList<V> path, ArrayList<LinkedList<V>> paths, TreeMap<V, Boolean> mustVisit) {
        if (vOrig.equals(vDest)) {
            for(Boolean visited : mustVisit.values()){
                if(!visited)
                    return;
            }
            
            paths.add((LinkedList<V>) path.clone());
            return; 
        } 
  
        for (V vAdj : g.adjVertices(vOrig)) {
            if (!path.contains(vAdj)) {
                path.add(vAdj);
                if(mustVisit.containsKey(vAdj))
                    mustVisit.put(vAdj, true);
                
                allPathsWithMustVisit(g, vAdj, vDest, path, paths, mustVisit);
                
                if(mustVisit.containsKey(vAdj))
                    mustVisit.put(vAdj, false);
                path.remove(vAdj);
            }
        }
    }

    /**
     * @param g Graph instance
     * @param vOrig information of the Vertex origin
     * @param vDest information of the Vertex destination
     * @return paths ArrayList with all paths from voInf to vdInf
     */
    public static <V, E> ArrayList<LinkedList<V>> allPathsWithMustVisit(Graph<V, E> g, V vOrig, V vDest, TreeMap<V, Boolean> mustVisit) {
        if (!g.validVertex(vOrig) || !g.validVertex(vDest)) {
            return null;
        }
        
        LinkedList<V> path = new LinkedList<>();
        ArrayList<LinkedList<V>> paths = new ArrayList<>();
        
        path.add(vOrig);
        allPathsWithMustVisit(g, vOrig, vDest, path, paths, mustVisit);
        
        return paths;
    }

    /**
     * Computes shortest-path distance from a source vertex to all reachable
     * vertices of a graph g with nonnegative edge weights This implementation
     * uses Dijkstra's algorithm
     *
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param visited set of discovered vertices
     * @param pathkeys minimum path vertices keys
     * @param dist minimum distances
     */
    protected static <V, E> void shortestPathLength(Graph<V, E> g, V vOrig,
            boolean[] visited, V[] pathKeys, double[] dist) {
        
        
        Hashtable<Integer, V> vIndex = new Hashtable<Integer, V>();
        int index = 0;
        int minDist = 0;
        
        for (V vert : g.vertices()){
            index = g.getKey(vert);
            if (vert == vOrig){
                visited[index] = true;
                dist[index] = 0;
                minDist = index;
            }else{
                visited[index] = false;
                dist[index] = Double.MAX_VALUE;
            }
            
            vIndex.put(index, vert);
        }
      
        
        V vControl = vOrig;
        while(minDist != -1){
            
            visited[minDist] = true;
            
            for (V vAdj : g.adjVertices(vControl)){
                Edge<V, E> ed = g.getEdge(vControl, vAdj);
                if(!visited[g.getKey(vAdj)] && dist[g.getKey(vAdj)]>dist[minDist]+ed.getWeight()){
                    dist[g.getKey(vAdj)] = dist[minDist] + ed.getWeight();
                    pathKeys[g.getKey(vAdj)] = vControl;
                }
            }
            
            minDist = findMinDistance(dist, visited);
            if (minDist != -1)
                vControl = vIndex.get(minDist);
        }
        
        return;
    }

    /**
     * Extracts from pathKeys the minimum path between voInf and vdInf The path
     * is constructed from the end to the beginning
     *
     * @param g Graph instance
     * @param voInf information of the Vertex origin
     * @param vdInf information of the Vertex destination
     * @param pathkeys minimum path vertices keys
     * @param path stack with the minimum path (correct order)
     */
    protected static <V, E> void getPath(Graph<V, E> g, V vOrig, V vDest, V[] pathKeys, LinkedList<V> path) {

        
        if ((vOrig != vDest) && (pathKeys[g.getKey(vDest)] == null))
            return;
        
        path.push(vDest);
        
        V vControl = pathKeys[g.getKey(vDest)];
        while(vControl != null){
            if (vControl != null)
                path.push(vControl);
            
            vControl = pathKeys[g.getKey(vControl)];
            
        }
    }

    //shortest-path between vOrig and vDest
    public static <V, E> double shortestPath(Graph<V, E> g, V vOrig, V vDest, LinkedList<V> shortPath) {        
        double retValue = 0.0;
        shortPath.removeAll(shortPath);
        
        if (!(g.validVertex(vOrig) && g.validVertex(vDest)))
            return retValue;
        
        boolean[] visited = new boolean[g.numVertices()] ;
        V[] pathKeys = (V[]) new Object[g.numVertices()];
        double[] dist = new double[g.numVertices()];
        shortestPathLength(g, vOrig, visited, pathKeys, dist);
        
        retValue = dist[g.getKey(vDest)];
        getPath(g, vOrig, vDest, pathKeys, shortPath);
        
        if (retValue == Double.MAX_VALUE)
            retValue = 0.0;
        
        return retValue;
    }

    //shortest-path between voInf and all other
    public static <V, E> boolean shortestPaths(Graph<V, E> g, V vOrig, ArrayList<LinkedList<V>> paths, ArrayList<Double> dists) {
        if (!g.validVertex(vOrig)) 
            return false; 

        int numVertices = g.numVertices();
        V[] pathKeys = (V[]) new Object[numVertices];
        double[] dist = new double[numVertices];
        boolean[] visited = new boolean[numVertices];

        for (int i = 0; i < numVertices; i++) {
            dist[i] = Double.MAX_VALUE;
            pathKeys[i] = null;
        }

        shortestPathLength(g, vOrig, visited, pathKeys, dist);

        dists.clear();
        paths.clear();

        for (int i = 0; i < numVertices; i++) {
            paths.add(null);
            dists.add(null);
        }

        int i = 0;
        for (V vDest : g.vertices()){
            LinkedList<V> shortPath = new LinkedList<>();
            if (dist[i] != Double.MAX_VALUE) {
                getPath(g, vOrig, vDest, pathKeys, shortPath);
            }
            paths.set(i, shortPath);
            dists.set(i, dist[i]);

            i++;
        }

        return true;
    }
    
    private static int findMinDistance(double[] distance, boolean[] visitedVertex) {
        double minDistance = Double.MAX_VALUE;
        int minDistanceVertex = -1;
        for (int i = 0; i < distance.length; i++) {
          if (!visitedVertex[i] && distance[i] < minDistance) {
            minDistance = distance[i];
            minDistanceVertex = i;
          }
        }
        return minDistanceVertex;
      }

    /**
     * Reverses the path
     *
     * @param path stack with path
     */
    private static <V, E> LinkedList<V> revPath(LinkedList<V> path) {

        LinkedList<V> pathcopy = new LinkedList<>(path);
        LinkedList<V> pathrev = new LinkedList<>();

        while (!pathcopy.isEmpty()) {
            pathrev.push(pathcopy.pop());
        }

        return pathrev;
    }
   
}
