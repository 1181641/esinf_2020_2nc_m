/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import data.reader;
import graphbase.Edge;
import graphbase.Graph;
import graphbase.GraphAlgorithms;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import matrixbase.AdjacencyMatrixGraph;

/**
 *
 * @author hugoc
 */
public class Network {

    private static reader r;

    protected static reader getR() {
        return r;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            File pathFile = new File(System.getProperty("user.dir"));
            pathFile = new File(pathFile, "src");
            pathFile = new File(pathFile, "data");

            r = new reader(pathFile);

            System.out.println("------------------ EX02 ------------------");
            ex02(r.getRelationships(), 7);
            System.out.println("------------------ EX03 ------------------");
            ex03(r.getRelationships(), r.getUsers());
            System.out.println("------------------ EX04 ------------------");
            ex04(r.getCountries(), r.getBorders(), r.getUsers(), r.getRelationships(), "u1", 2);
            System.out.println("------------------ EX05 ------------------");
            ex05(r.getCountries(), r.getUsers(), r.getBorders(), 10, 2);
            System.out.println("------------------ EX06 ------------------");
            LinkedList<String> shortPath = new LinkedList<String>();
            ex06(r.getCountries(), r.getUsers(), r.getBorders(),r.getRelationships(),"u668", "u190", 2, shortPath);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(Network.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(Network.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static SortedMap<AbstractMap.SimpleEntry<String, Integer>, Integer> getMostPopularUsers(AdjacencyMatrixGraph<String, Double> relationships, int nUtilizadores) {

        SortedMap<AbstractMap.SimpleEntry<String, Integer>, Integer> friendships = new TreeMap<>((o1, o2) -> {
            return o2.getValue().compareTo(o1.getValue());
        });

        int numFriends;
        for (String vObj : relationships.vertices()) {
            numFriends = relationships.outDegree(vObj);

            if (numFriends > 0) {
                AbstractMap.SimpleEntry<String, Integer> newKey = new AbstractMap.SimpleEntry<>(vObj, numFriends);
                if (friendships.size() != nUtilizadores) {
                    friendships.put(newKey, 1);
                } else {
                    AbstractMap.SimpleEntry<String, Integer> last = friendships.lastKey();
                    if (numFriends > last.getValue()) {
                        friendships.remove(last);
                        friendships.put(newKey, 1);
                    }
                }
            }
        }

        return friendships;
    }

    public static <K, V extends Comparable<V>> ArrayList<String> ex02(AdjacencyMatrixGraph<String, Double> relationships, int nUtilizadores) {

        SortedMap<AbstractMap.SimpleEntry<String, Integer>, Integer> friendships = getMostPopularUsers(relationships, nUtilizadores);
        ArrayList<String> lstCommunFriends = new ArrayList<>();

        TreeMap<String, String[]> common = new TreeMap<>();
        AbstractMap.SimpleEntry<String, Integer> key;
        String[] friends;
        Boolean isCommon;
        Integer index;
        for (String vObj : relationships.vertices()) {
            friends = new String[friendships.size()];
            isCommon = true;
            index = 0;
            for (Map.Entry<AbstractMap.SimpleEntry<String, Integer>, Integer> currentEntry : friendships.entrySet()) {
                key = currentEntry.getKey();

                if (relationships.getEdge(key.getKey(), vObj) != null) {
                    friends[index] = key.getKey();
                    index++;
                } else {
                    isCommon = false;
                }
            }
            if (isCommon) {
                lstCommunFriends.add(vObj);
                common.put(vObj, friends);
                System.out.println(vObj + " - " + Arrays.toString(friends));
            }
        }
        return lstCommunFriends;
    }

    /**
     * Verifica se o graph é conetado - Todos os vertices conseguem aceder a
     * qualquer vértice
     *
     * @param relationships Graph a ser verificado
     * @param vOrig Vértice origem
     * @return
     */
    public static boolean isConnected(AdjacencyMatrixGraph<String, Double> relationships, String vOrig) {
        LinkedList<String> resultQueue = matrixbase.GraphAlgorithms.BFS(relationships, vOrig);
        if (resultQueue == null) {
            return false;
        }
        System.out.println("vertices count: " + relationships.numVertices() + " | BFS count: " + resultQueue.size());
        boolean isConnected = (resultQueue.size() == relationships.numVertices());
        return isConnected;
    }

    /**
     *
     * @param relationships
     * @param users
     * @return NULL em caso de não ser conecto, em caso de ser connecto o número
     * minimo de ligações para todos verem todos
     */
    public static Integer ex03(AdjacencyMatrixGraph<String, Double> relationships, TreeMap<String, User> users) {
        Integer retValue = null;

        if (isConnected(relationships, users.firstKey())) {
            LinkedList<String> ligacoesMinimas = matrixbase.GraphAlgorithms.MST(relationships);
            retValue = ligacoesMinimas.size();
            System.out.println("Minimun connections: " + retValue);
        } else {
            System.out.println("Unconnected graph!");
        }

        return retValue;
    }

    /**
     * Buscar todas as fronteiras do país de um utilizador
     *
     * @param countries
     * @param borders
     * @param u User
     * @param nFronteiras níveis de fronteiras
     * @return TreeMap com fronteiras e amizades de cada fronteira
     */
    public static TreeMap<String, String> getBorders(TreeMap<String, Country> countries, Graph<String, String> borders, User u, int nFronteiras) {
        if (nFronteiras < 1) {
            return null;
        }

        Country c;
        TreeMap<String, String> allCountries = new TreeMap<>();
        TreeMap<String, String> bordas = new TreeMap<>();

        for (Country item : countries.values()) {
            if (bordas.isEmpty()) {
                allCountries.put(item.getCountry(), item.getCapital());
            }

            if (item.getCapital().equals(u.getCity())) {
                c = item;

                bordas.put(c.getCountry(), c.getCapital());

                for (String pais : borders.adjVertices(c.getCountry())) {
                    if (allCountries.containsKey(pais)) {
                        bordas.put(pais, allCountries.get(pais));
                    } else {
                        bordas.put(pais, null);
                    }
                }

                for (int i = 1; i < nFronteiras; i++) {
                    for (Object key : bordas.keySet().toArray()) {

                        for (Object pais : borders.adjVertices((String) key)) {
                            String b = (String) pais;
                            if (allCountries.containsKey(b)) {
                                bordas.put(b, allCountries.get(b));
                            } else {
                                bordas.put(b, null);
                            }
                        }

                    }
                }
            }

            if (bordas.containsKey(item.getCountry())) {
                bordas.put(item.getCountry(), item.getCapital());
            }
        }

        return bordas;
    }

    public static TreeMap<String, String[]> ex04(TreeMap<String, Country> countries, Graph<String, String> borders, TreeMap<String, User> users, AdjacencyMatrixGraph<String, Double> relationships, String utilizador, int nFronteiras) {
        User u = users.get(utilizador);
        if (u != null) {
            TreeMap<String, String> bordas = getBorders(countries, borders, u, nFronteiras);

            User friend;
            TreeMap<String, String[]> countryFriends = new TreeMap<>();
            for (String con : relationships.directConnections(u.getUser())) {
                friend = users.get(con);

                if (bordas.containsValue(friend.getCity())) {
                    String[] arr = new String[1];
                    if (countryFriends.get(friend.getCity()) != null) {
                        arr = countryFriends.get(friend.getCity());
                        arr = Arrays.copyOf(arr, arr.length + 1);
                    }
                    arr[(arr.length - 1)] = friend.getUser();
                    countryFriends.put(friend.getCity(), arr);
                }
            }

            for (Map.Entry<String, String[]> currentEntry : countryFriends.entrySet()) {
                System.out.println(currentEntry.getKey() + " - " + Arrays.toString(currentEntry.getValue()));
            }

            return countryFriends;
        }
        return null;
    }

    /**
     * Buscar a percentagem de utilizadores por país
     *
     * @param countries TreeMap countries
     * @param users TreeMap users
     * @param pUtilizadores Percentagem de utilizadores desejada
     * @return Map com país e a sua percentagem de utilizadores.
     */
    public static TreeMap<String, Float> getCitiesByUserPercetage(TreeMap<String, Country> countries, TreeMap<String, User> users, float pUtilizadores) {
        // Verificar cidades cujo percetagem de utilizadores é igual ou superior a pUtilizadores
        int totalUsers = users.size();

        TreeMap<String, String> tmpCities = new TreeMap<>();
        for (String country : countries.keySet()) {
            tmpCities.put(countries.get(country).getCapital(), countries.get(country).getCountry());
        }

        TreeMap<String, Float> cities = new TreeMap<>();

        int totalUsersCountry;
        for (String city : tmpCities.keySet()) {
            totalUsersCountry = 0;
            for (String user : users.keySet()) {
                if (users.get(user).getCity().equals(city)) {
                    totalUsersCountry++;
                }
            }

            float pUsersCountry = totalUsersCountry * 100 / totalUsers;
            if (pUsersCountry >= pUtilizadores) {
                cities.put(tmpCities.get(city), pUsersCountry);
            }
        }

        return cities;
    }

    /**
     * Buscar a média de distâncias de todos os países para todos os outros
     *
     * @param countries
     * @param users
     * @param borders
     * @param nCidades
     * @param pUtilizadores
     * @return Map com país e a sua média de distância para todos os outros
     * países
     */
    public static TreeMap<String, Double> getAverageDistance(TreeMap<String, Country> countries, TreeMap<String, User> users, Graph<String, String> borders, int nCidades, float pUtilizadores) {
        TreeMap<String, Float> citiesUserPercentage = getCitiesByUserPercetage(countries, users, pUtilizadores);
        TreeMap<String, Double> citiesDist = new TreeMap<>();

        ArrayList<LinkedList<String>> paths;
        ArrayList<Double> dists;

        Graph<String, String> myBorders = new Graph<>(false);
        for (String item : borders.vertices()) {
            myBorders.insertVertex(item);
        }

        ArrayList<String> tmpLst = new ArrayList<>();
        for (String vOrig : myBorders.vertices()) {
            for (String vDest : myBorders.vertices()) {
                if (!vOrig.equals(vDest) && !tmpLst.contains(vDest)) {
                    myBorders.insertEdge(vOrig, vDest, null, Country.calcularDistancia(countries.get(vOrig), countries.get(vDest)));
                }
            }
            tmpLst.add(vOrig);
        }

        for (String city : citiesUserPercentage.keySet()) {
            paths = new ArrayList<>();
            dists = new ArrayList<>();
            if (GraphAlgorithms.shortestPaths(myBorders, city, paths, dists)) {
                Double dist = 0D;
                for (int i = 0; i < dists.size(); i++) {
                    dist += dists.get(i);
                }
                dist = dist / (dists.size() - 1);
                citiesDist.put(city, dist);
            }
        }

        return citiesDist;
    }

    /**
     * Cidades com maior centralidade
     *
     * @param countries Map de países
     * @param users Map de utilizadores
     * @param borders Graph de fronteiras entre países
     * @param nCidades número de cidades
     * @param pUtilizadores percentagem de utilizadores na cidade
     * @return Map com país e a sua média de distância
     */
    public static TreeMap<String, Double> ex05(TreeMap<String, Country> countries, TreeMap<String, User> users, Graph<String, String> borders, int nCidades, float pUtilizadores) {
        TreeMap<String, Double> citiesDist = getAverageDistance(countries, users, borders, nCidades, pUtilizadores);
        TreeMap<String, Double> mapCities = new TreeMap<>();

        for (String city : citiesDist.keySet()) {
            Double distance = citiesDist.get(city);
            if (mapCities.size() != nCidades) {
                mapCities.put(city, distance);
            } else {
                String maxKey = "";
                Double maxDist = 0D;
                for (String key : mapCities.keySet()) {
                    if (mapCities.get(key) > maxDist) {
                        maxDist = mapCities.get(key);
                        maxKey = key;
                    }
                }

                if (distance < mapCities.get(maxKey)) {
                    mapCities.remove(maxKey);
                    mapCities.put(city, distance);
                }
            }
        }

        for (String x : mapCities.keySet()) {
            System.out.println(x + " - " + mapCities.get(x));
        }

        return mapCities;
    }

    public static <V, E> Double ex06(TreeMap<String, Country> rCountries, 
            TreeMap<String, User> rUsers, 
            Graph<String, String> rBorders,
            AdjacencyMatrixGraph<String, Double> rRelationships,
            String vOrig, 
            String vDest, 
            int numCidades, 
            LinkedList<String> shortPath) {

        Double retvalue = null;
        
        if ((!rUsers.containsKey(vOrig)) || (!rUsers.containsKey(vDest))) {
            return retvalue;
        }

        User uOrigem = rUsers.get(vOrig);
        User uDestino = rUsers.get(vDest);

        String[] listUsers = {vOrig, vDest};
        List<reader.friendsPerCity> listaAmigosPorCidade;
        List<Object> listCidades = new ArrayList<>();

        Country c;
        reader.friendsPerCity friendPerCity;
        for (String vUser : listUsers) {
            listaAmigosPorCidade = new ArrayList<>();

            for (String vFriend : rRelationships.directConnections(vUser)) {
                String city = rUsers.get(vFriend).getCity();
                if (listaAmigosPorCidade.stream().anyMatch(p -> p.getCity().equals(city))) {
                    friendPerCity = listaAmigosPorCidade.stream().filter(p -> p.getCity().equals(city)).findFirst().get();
                    friendPerCity.setNumFriends(friendPerCity.getNumFriends() + 1);
                    listaAmigosPorCidade.removeIf(p -> p.getCity().equals(city));
                    listaAmigosPorCidade.add(friendPerCity);
                } else {
                    friendPerCity = r.new friendsPerCity(city, 1);
                    listaAmigosPorCidade.add(friendPerCity);
                }
            }

            listaAmigosPorCidade.sort((o1, o2) -> {
                return o2.getNumFriends().compareTo(o1.getNumFriends()); //To change body of generated lambdas, choose Tools | Templates.
            });

            listCidades.addAll(Arrays.asList(listaAmigosPorCidade.subList(0, numCidades).stream().map(m -> m.getCity()).toArray()));
        }
        listCidades = Arrays.asList(listCidades.stream().distinct().toArray());
        TreeMap<String, Boolean> mustVisit = new TreeMap<>();
        String cOrig = null, cDest = null;
        for (Country item : rCountries.values()) {

            if (listCidades.contains(item.getCapital()) && (!item.getCapital().equals(uOrigem.getCity()) && !item.getCapital().equals(uDestino.getCity()))) {
                mustVisit.put(item.getCountry(), false);
            }

            if (item.getCapital().equals(uOrigem.getCity())) {
                cOrig = item.getCountry();
            }

            if (item.getCapital().equals(uDestino.getCity())) {
                cDest = item.getCountry();
            }

            if ((listCidades.size() == mustVisit.size()) && (cDest != null) && (cOrig != null)) {
                break;
            }

        }

        Graph<String, String> tempGraph = new Graph<>(false);
        String vPrevious = null;
        shortPath.clear();
        Double minWeight = null;
        Boolean isEmpty;
        if(mustVisit.size() != 0){
            for (Object item : mustVisit.keySet().toArray()) {
                String key = (String) item;
                isEmpty = true;

                GraphAlgorithms.shortestPath(rBorders, cOrig, key, shortPath);
                if (!shortPath.isEmpty()) {
                    isEmpty = false;
                }
                for (String v : shortPath) {
                    if (!tempGraph.validVertex(v)) {
                        tempGraph.insertVertex(v);
                    }

                    if (vPrevious != null) {
                        Edge<String, String> ed = rBorders.getEdge(vPrevious, v);
                        if (ed != null) {
                            tempGraph.insertEdge(vPrevious, v, ed.getElement(), ed.getWeight());
                        }
                    }
                    vPrevious = v;
                }

                GraphAlgorithms.shortestPath(rBorders, key, cDest, shortPath);
                if (!shortPath.isEmpty()) {
                    isEmpty = false;
                }
                vPrevious = null;
                for (String v2 : shortPath) {
                    if (!tempGraph.validVertex(v2)) {
                        tempGraph.insertVertex(v2);
                    }

                    if (vPrevious != null) {
                        Edge<String, String> ed = rBorders.getEdge(vPrevious, v2);
                        tempGraph.insertEdge(vPrevious, v2, ed.getElement(), ed.getWeight());
                    }
                    vPrevious = v2;
                }

                if (isEmpty) {
                    mustVisit.remove(item);
                }
            }

            boolean inserted = true;
            while (inserted) {
                inserted = false;
                Iterable<String> vertices = tempGraph.clone().vertices();
                for (String item1 : vertices) {
                    for (String item2 : vertices) {
                        GraphAlgorithms.shortestPath(rBorders, (String) item1, (String) item2, shortPath);
                        vPrevious = null;
                        for (String v2 : shortPath) {
                            if (!tempGraph.validVertex(v2)) {
                                tempGraph.insertVertex(v2);
                            }

                            if (vPrevious != null) {
                                Edge<String, String> ed = rBorders.getEdge(vPrevious, v2);
                                if (ed != null) {
                                    if (tempGraph.insertEdge(vPrevious, v2, ed.getElement(), ed.getWeight())) {
                                        inserted = true;
                                    }
                                }
                            }
                            vPrevious = v2;
                        }
                    }
                }
            }

            ArrayList<LinkedList<String>> allPath = GraphAlgorithms.allPathsWithMustVisit(tempGraph, cOrig, cDest, mustVisit);
            shortPath = null;
            minWeight = Double.MAX_VALUE;
            if (!allPath.isEmpty()) {
                double[] dist = new double[allPath.size()];
                for (LinkedList<String> path : allPath) {
                    vPrevious = null;
                    for (String item : path) {
                        if (vPrevious != null) {
                            dist[allPath.indexOf(path)] += tempGraph.getEdge(vPrevious, item).getWeight();
                        }
                        vPrevious = item;
                    }

                    if (dist[allPath.indexOf(path)] < minWeight) {
                        shortPath = path;
                        minWeight = dist[allPath.indexOf(path)];
                    }
                }
            }
        }else{
            minWeight = GraphAlgorithms.shortestPath(rBorders, cOrig, cDest, shortPath);
        }
        
        if (shortPath != null && !shortPath.isEmpty()) {
            for (String pais : shortPath) {
                pais = rCountries.get(pais).getCapital();
                System.out.println(pais);
            }
            System.out.println("Total Km: " + minWeight);
            retvalue = minWeight;
        } else {
            shortPath.clear();
            retvalue = null;
            System.out.println("Não foi possivel encontrar um caminho com as cidades indicadas.");
        }
        
        return minWeight;
    }
}
