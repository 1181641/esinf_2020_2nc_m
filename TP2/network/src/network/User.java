/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

/**
 *
 * @author hugoc
 */
public class User {
    
    private String user;
    private int age;
    private String city;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public User() {
    }

    public User(String user, int age, String city) {
        this.user = user;
        this.age = age;
        this.city = city;
    }

    @Override
    public String toString() {
        return "User{" + "user=" + user + ", age=" + age + ", city=" + city + '}';
    }
    
    
}
