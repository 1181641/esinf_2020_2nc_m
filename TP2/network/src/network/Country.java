/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

/**
 *
 * @author hugoc
 */
public class Country {

    private String country;
    private String continent;
    private float population;
    private String capital;
    private float latitude;
    private float longitude;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public float getPopulation() {
        return population;
    }

    public void setPopulation(float population) {
        this.population = population;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public Country() {
    }

    public Country(String country, String continent, float population, String capital, float latitude, float longitude) {
        this.country = country;
        this.continent = continent;
        this.population = population;
        this.capital = capital;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /**
     * Calcular a distância entre dois países
     * @param c1
     * @param c2
     * @return 
     */
    public static Double calcularDistancia(Country c1, Country c2) {
        if (c1.getLatitude() == 0.0F || c1.getLongitude() == 0.0F || c2.getLatitude() == 0.0F || c2.getLongitude() == 0.0F) {
            return null;
        }
        return calcularDistancia(c1.latitude, c2.latitude, c1.longitude, c2.longitude);
    }

    private static double calcularDistancia(float lat1, float lat2, float lon1,
            float lon2) {
        final double R = 6371e3; // metres

        double lat1Distance = lat1 * Math.PI / 180; // lat1 in radians
        double lat2Distance = lat2 * Math.PI / 180; // lat2 in radians
        double deltaLat = (lat2 - lat1) * Math.PI / 180;
        double deltaLon = (lon2 - lon1) * Math.PI / 180;
        double a = Math.sin(deltaLat / 2) * Math.sin(deltaLat / 2)
                + Math.cos(lat1Distance) * Math.cos(lat2Distance)
                * Math.sin(deltaLon / 2) * Math.sin(deltaLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c / 1000; // in kms

        return distance;
    }

}
