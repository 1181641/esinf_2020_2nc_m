/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import graphbase.Graph;
import matrixbase.AdjacencyMatrixGraph;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import network.Country;
import network.User;


/**
 *
 * @author (1181312) Hugo Costa | (1181641) Tiago Santos
 */
public class reader {

    private final String borderFile = "borders.txt";
    private final String countriesFile = "countries.txt";
    private final String relationshipsFile = "relationships.txt";
    private final String usersFile = "users.txt";

    
    
    private File pathFile;
    private TreeMap<String, User> users = new TreeMap<>();
    private AdjacencyMatrixGraph<String, Double> relationships = new AdjacencyMatrixGraph<>();
    private TreeMap<String, Country> countries = new TreeMap<>();
    private Graph<String, String> borders = new Graph<>(false);

    public TreeMap<String, User> getUsers() {
        return users;
    }
    
    public AdjacencyMatrixGraph<String, Double> getRelationships(){
        return relationships;
    }

    public TreeMap<String, Country> getCountries() {
        return countries;
    }

    public Graph<String, String> getBorders() {
        return borders;
    }

    public reader(TreeMap<String, User> users, AdjacencyMatrixGraph<String, Double> relationships, TreeMap<String, Country> countries, Graph<String, String> borders){
        this.users = users;
        this.relationships = relationships;
        this.countries = countries;
        this.borders = borders;
    }
    
    public reader(File pathFile) throws FileNotFoundException, ParseException {
        this.pathFile = pathFile;
        readUsers();
        readRelationships();
        readCountries();
        readBorders();
    }

    /**
     * Ler utilizadores a partir do ficheiro fornecido e atribuir a TreeMap
     * "users"
     */
    private void readUsers() {
        File srcPath = new File(pathFile, usersFile);
        String filePath = srcPath.getPath();
        BufferedReader br = null;
        String line = "";
        String splitBy = ",";

        try {
            br = new BufferedReader(new FileReader(filePath));

            users = new TreeMap<>();
            while ((line = br.readLine()) != null) {
                String[] arrayData = line.replace("\"", "").split(splitBy);

                User newItem = new User();
                newItem.setUser(arrayData[0].trim());
                newItem.setAge(Integer.parseInt(arrayData[1].trim()));
                newItem.setCity(arrayData[2].trim());

                users.put(newItem.getUser(), newItem);
                relationships.insertVertex(newItem.getUser());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(reader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(reader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    /**
     * Ler relacoes a partir do ficheiro fornecido e atribuir a AdjacencyMatrixGraph
     * "readRelationships"
     */
    private void readRelationships() {
        File srcPath = new File(pathFile, relationshipsFile);
        String filePath = srcPath.getPath();
        BufferedReader br = null;
        String line = "";
        String splitBy = ",";

        try {
            br = new BufferedReader(new FileReader(filePath));

            while ((line = br.readLine()) != null) {
                String[] arrayData = line.replace("\"", "").split(splitBy);
                
                String c1, c2;
                c1 = arrayData[0].trim();
                c2 = arrayData[1].trim();
                
                if (users.get(c1) != null && users.get(c2) != null) {
                    relationships.insertEdge(c1, c2, Double.valueOf(1));
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(reader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(reader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    
    /**
     * Ler países a partir do ficheiro fornecido e atribuir a TreeMap
     * "countries"
     */
    private void readCountries() {
        File srcPath = new File(pathFile, countriesFile);
        String filePath = srcPath.getPath();
        BufferedReader br = null;
        String line = "";
        String splitBy = ",";

        try {
            br = new BufferedReader(new FileReader(filePath));

            countries = new TreeMap<>();
            while ((line = br.readLine()) != null) {
                String[] arrayData = line.replace("\"", "").split(splitBy);

                Country newItem = new Country();
                newItem.setCountry(arrayData[0].trim());
                newItem.setContinent(arrayData[1].trim());
                newItem.setPopulation(Float.parseFloat(arrayData[2].trim()));
                newItem.setCapital(arrayData[3].trim());
                newItem.setLatitude(Float.parseFloat(arrayData[4].trim()));
                newItem.setLongitude(Float.parseFloat(arrayData[5].trim()));

                countries.put(newItem.getCountry(), newItem);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(reader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(reader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Ler fronteiras a partir do ficheiro fornecido e atribuir ao Graph "borders"
     */
    private void readBorders() {
        File srcPath = new File(pathFile, borderFile);
        String filePath = srcPath.getPath();
        BufferedReader br = null;
        String line = "";
        String splitBy = ",";

        try {
            br = new BufferedReader(new FileReader(filePath));
            
            for (String key : countries.keySet()) {
                borders.insertVertex(key);
            }

            int i = 0;
            while ((line = br.readLine()) != null) {
                String[] arrayData = line.replace("\"", "").split(splitBy);

                String c1, c2;
                c1 = arrayData[0].trim();
                c2 = arrayData[1].trim();

                if (countries.get(c1) != null && countries.get(c2) != null) {
                    double distance = 0.0;
                    Country country1 = countries.get(c1);
                    Country country2 = countries.get(c2);

                    //Calcular distancia entre as capitais
                    distance = Country.calcularDistancia(country1, country2);
                    borders.insertEdge(c1, c2, String.valueOf(i), distance);
                    i++;
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(reader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(reader.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    
    public class friendsPerCity{
        String city;
        Integer numFriends;

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public Integer getNumFriends() {
            return numFriends;
        }

        public void setNumFriends(Integer numFriends) {
            this.numFriends = numFriends;
        }

        public friendsPerCity() {
        }
        
        public friendsPerCity(String city, Integer numFriends) {
            this.city = city;
            this.numFriends = numFriends;
        }   
    }
}
