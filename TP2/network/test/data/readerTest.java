/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import graphbase.Graph;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.TreeMap;
import matrixbase.AdjacencyMatrixGraph;
import network.Country;
import network.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author hugoc
 */
public class readerTest {

    private reader bigNetwork;
    private reader smallNetwork;
    private reader myNetwork;

    public readerTest() {
    }

    @Before
    public void setUp() {

        File pathFile = new File(System.getProperty("user.dir"));
        pathFile = new File(pathFile, "src");
        pathFile = new File(pathFile, "data");

        try {
            File bigPath = new File(pathFile, "big-network");
            bigNetwork = new reader(bigPath);
        } catch (FileNotFoundException ex) {
            assertFalse("BigNetwork - FileNotFoundException.\n" + ex.getMessage(), true);
        } catch (ParseException ex) {
            assertFalse("BigNetwork - Parse exception.\n" + ex.getMessage(), true);
        }

        try {
            File smallPath = new File(pathFile, "small-network");
            smallNetwork = new reader(smallPath);
        } catch (FileNotFoundException ex) {
            assertFalse("SmallNetwork - FileNotFoundException.\n" + ex.getMessage(), true);
        } catch (ParseException ex) {
            assertFalse("SmallNetwork - Parse exception.\n" + ex.getMessage(), true);
        }

        TreeMap<String, User> users = new TreeMap<>();
        AdjacencyMatrixGraph<String, Double> relationships = new AdjacencyMatrixGraph<>();
        TreeMap<String, Country> countries = new TreeMap<>();
        Graph<String, String> borders = new Graph<>(false);

        users.put("u1", new User("u1", 25, "lisboa"));
        users.put("u2", new User("u1", 25, "madrid"));
        users.put("u3", new User("u1", 25, "paris"));
        users.put("u4", new User("u1", 25, "lisboa"));
        users.put("u5", new User("u1", 25, "lisboa"));
        users.put("u6", new User("u1", 25, "lisboa"));
        users.put("u7", new User("u1", 25, "lisboa"));
        users.put("u8", new User("u1", 25, "lisboa"));
        users.put("u9", new User("u1", 25, "lisboa"));

        for (String key : users.keySet()) {
            relationships.insertVertex(key);
        }
        
        relationships.insertEdge("u1", "u2", 1D);
        relationships.insertEdge("u1", "u2", 1D);
        relationships.insertEdge("u1", "u3", 1D);
        relationships.insertEdge("u1", "u4", 1D);
        relationships.insertEdge("u1", "u5", 1D);
        relationships.insertEdge("u1", "u6", 1D);
        relationships.insertEdge("u1", "u7", 1D);
        relationships.insertEdge("u1", "u8", 1D);
        relationships.insertEdge("u2", "u3", 1D);
        relationships.insertEdge("u2", "u4", 1D);
        relationships.insertEdge("u2", "u5", 1D);
        relationships.insertEdge("u2", "u6", 1D);
        relationships.insertEdge("u3", "u4", 1D);
        relationships.insertEdge("u3", "u5", 1D);
        relationships.insertEdge("u4", "u5", 1D);
        relationships.insertEdge("u8", "u9", 1D);
        
        Country portugal = new Country("portugal", "europa", 10.31F, "lisboa", 38.7071631F, -9.135517F);
        Country spain = new Country("espanha", "europa", 46.53F, "madrid", 40.4166909F, -3.7003454F);
        Country france = new Country("franca", "europa", 66.99F, "paris", 48.8566667F, 2.3509871F);
        countries.put("portugal", portugal);
        countries.put("espanha", spain);
        countries.put("franca", france);
        
        for (String key : countries.keySet()) {
            borders.insertVertex(key);
        }
        Double dSP = Country.calcularDistancia(spain, portugal);
        Double dSF = Country.calcularDistancia(spain, france);
        borders.insertEdge(portugal.getCountry(), spain.getCountry(), null, dSP);
        borders.insertEdge(france.getCountry(), spain.getCountry(), null, dSF);
        
        myNetwork = new reader(users, relationships, countries, borders);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getUsers method, of class reader.
     */
    @Test
    public void testGetUsers() {
        System.out.println("getUsers");

        //smallNetwork
        assertTrue("SmallNetwork - Users should be <26> but it was <" + smallNetwork.getUsers().size() + ">.",
                smallNetwork.getUsers().size() == 26);
        assertTrue("SmallNetwork - First entry should be <u1> but it was <" + smallNetwork.getUsers().firstKey() + ">.",
                smallNetwork.getUsers().firstKey().equals("u1"));
        assertTrue("SmallNetwork - First entry should be <u9> but it was <" + smallNetwork.getUsers().lastKey() + ">.",
                smallNetwork.getUsers().lastKey().equals("u9"));

        //bigNetwork
        assertTrue("BigNetwork - Users should be <769> but it was <" + bigNetwork.getUsers().size() + ">.",
                bigNetwork.getUsers().size() == 769);
        assertTrue("BigNetwork - First entry should be <u1> but it was <" + bigNetwork.getUsers().firstKey() + ">.",
                bigNetwork.getUsers().firstKey().equals("u1"));
        assertTrue("BigNetwork - First entry should be <u99> but it was <" + bigNetwork.getUsers().lastKey() + ">.",
                bigNetwork.getUsers().lastKey().equals("u99"));
        
        //MyNetwork
        assertTrue("MyNetwork - Users should be <9> but it was <" + myNetwork.getUsers().size() + ">.",
                myNetwork.getUsers().size() == 9);
        assertTrue("MyNetwork - First entry should be <u1> but it was <" + myNetwork.getUsers().firstKey() + ">.",
                myNetwork.getUsers().firstKey().equals("u1"));
        assertTrue("MyNetwork - First entry should be <u9> but it was <" + myNetwork.getUsers().lastKey() + ">.",
                myNetwork.getUsers().lastKey().equals("u9"));
    }

    /**
     * Test of getCountries method, of class reader.
     */
    @Test
    public void testGetCountries() {
        System.out.println("getCountries");

        //smallNetwork
        assertTrue("SmallNetwork - Countries should be <13> but it was <" + smallNetwork.getCountries().size() + ">.",
                smallNetwork.getCountries().size() == 13);
        assertTrue("SmallNetwork - First entry should be <argentina> but it was <" + smallNetwork.getCountries().firstKey() + ">.",
                smallNetwork.getCountries().firstKey().equals("argentina"));

        //BigNetwork
        assertTrue("BigNetwork - Countries should be <59> but it was <" + bigNetwork.getCountries().size() + ">.",
                bigNetwork.getCountries().size() == 59);
        assertTrue("BigNetwork - First entry should be <albania> but it was <" + bigNetwork.getCountries().firstKey() + ">.",
                bigNetwork.getCountries().firstKey().equals("albania"));
        
        //MyNetwork
        assertTrue("MyNetwork - Countries should be <3> but it was <" + myNetwork.getCountries().size() + ">.",
                myNetwork.getCountries().size() == 3);
        assertTrue("MyNetwork - First entry should be <espanha> but it was <" + myNetwork.getCountries().firstKey() + ">.",
                myNetwork.getCountries().firstKey().equals("espanha"));
    }

    /**
     * Test of getBorders method, of class reader.
     */
    @Test
    public void testGetBorders() {
        System.out.println("getBorders");

        //smallNetwork        
        assertTrue("SmallNetwork - Border vertices should be <13> but it was <" + smallNetwork.getBorders().numVertices() + ">.",
                smallNetwork.getBorders().numVertices() == 13);

        //bigNetwork        
        assertTrue("BigNetwork - Border vertices should be <59> but it was <" + bigNetwork.getBorders().numVertices() + ">.",
                bigNetwork.getBorders().numVertices() == 59);
        
        //MyNetwork        
        assertTrue("MyNetwork - Border vertices should be <3> but it was <" + myNetwork.getBorders().numVertices() + ">.",
                myNetwork.getBorders().numVertices() == 3);

    }

}
