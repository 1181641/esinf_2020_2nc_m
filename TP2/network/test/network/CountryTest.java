/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author hugoc
 */
public class CountryTest {

    Country c1 = new Country();

    public CountryTest() {
    }

    @Before
    public void setUp() {
        c1 = new Country("portugal", "europa", 10.31F, "lisboa", 38.7071631F, -9.135517F);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getCountry method, of class Country.
     */
    @Test
    public void testGetCountry() {
        System.out.println("getCountry");
        String expResult = "portugal";
        String result = c1.getCountry();
        assertTrue(expResult.equals(result));
    }

    /**
     * Test of setCountry method, of class Country.
     */
    @Test
    public void testSetCountry() {
        System.out.println("setCountry");
        String country = "brasil";
        Country instance = new Country();
        instance.setCountry(country);
        assertTrue(country.equals(instance.getCountry()));
    }

    /**
     * Test of getContinent method, of class Country.
     */
    @Test
    public void testGetContinent() {
        System.out.println("getContinent");
        String expResult = "europa";
        String result = c1.getContinent();
        assertTrue(expResult.equals(result));
    }

    /**
     * Test of setContinent method, of class Country.
     */
    @Test
    public void testSetContinent() {
        System.out.println("setContinent");
        String continent = "americasul";
        Country instance = new Country();
        instance.setContinent(continent);
        assertTrue(continent.equals(instance.getContinent()));
    }

    /**
     * Test of getPopulation method, of class Country.
     */
    @Test
    public void testGetPopulation() {
        System.out.println("getPopulation");
        float expResult = 10.31F;
        float result = c1.getPopulation();
        assertTrue(expResult == result);
    }

    /**
     * Test of setPopulation method, of class Country.
     */
    @Test
    public void testSetPopulation() {
        System.out.println("setPopulation");
        float population = 209.5F;
        Country instance = new Country();
        instance.setPopulation(population);
        assertTrue(population == instance.getPopulation());
    }

    /**
     * Test of getCapital method, of class Country.
     */
    @Test
    public void testGetCapital() {
        System.out.println("getCapital");
        String expResult = "lisboa";
        String result = c1.getCapital();
        assertTrue(expResult.equals(result));
    }

    /**
     * Test of setCapital method, of class Country.
     */
    @Test
    public void testSetCapital() {
        System.out.println("setCapital");
        String capital = "brasilia";
        Country instance = new Country();
        instance.setCapital(capital);
        assertTrue(capital.equals(instance.getCapital()));
    }

    /**
     * Test of getLatitude method, of class Country.
     */
    @Test
    public void testGetLatitude() {
        System.out.println("getLatitude");
        float expResult = 38.7071631F;
        float result = c1.getLatitude();
        assertTrue(expResult == result);
    }

    /**
     * Test of setLatitude method, of class Country.
     */
    @Test
    public void testSetLatitude() {
        System.out.println("setLatitude");
        float latitude = -15.7797200F;
        Country instance = new Country();
        instance.setLatitude(latitude);
        assertTrue(latitude == instance.getLatitude());
    }

    /**
     * Test of getLongitude method, of class Country.
     */
    @Test
    public void testGetLongitude() {
        System.out.println("getLongitude");
        float expResult = -9.135517F;
        float result = c1.getLongitude();
        assertTrue(expResult == result);
    }

    /**
     * Test of setLongitude method, of class Country.
     */
    @Test
    public void testSetLongitude() {
        System.out.println("setLongitude");
        float longitude = -47.9297200F;
        Country instance = new Country();
        instance.setLongitude(longitude);
        assertTrue(longitude == instance.getLongitude());
    }

    /**
     * Test of calcularDistancia method, of class Country.
     */
    @Test
    public void testCalcularDistancia() {
        System.out.println("calcularDistancia");

        Country c2 = new Country();
        // Teste null value
        Double rNull = Country.calcularDistancia(c1, c2);
        assertTrue("Preenchimento de latitude ou longitude em falta.", rNull == null);

        // Teste distância
        // Portugal -> Espanha
        c2.setLatitude(40.4166909F);
        c2.setLongitude(-3.7003454F);
        double expResult = 503.0967126399002;
        double result = Country.calcularDistancia(c1, c2);
        assertTrue("Expected <" + expResult + "> but it was <" + result + ">.", expResult == result);

        // Portugal -> Brasil
        c2.setLatitude(-15.7797200F);
        c2.setLongitude(-47.9297200F);
        expResult = 7279.494935333474;
        result = Country.calcularDistancia(c1, c2);
        assertTrue("Expected <" + expResult + "> but it was <" + result + ">.", expResult == result);
    }

}
