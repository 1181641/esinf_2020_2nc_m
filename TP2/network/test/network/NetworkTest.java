/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import data.reader;
import graphbase.Graph;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import matrixbase.AdjacencyMatrixGraph;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author hugoc
 */
public class NetworkTest {

    private Network instance;
    private reader myNetwork;

    public NetworkTest() {

    }

    @Before
    public void setUp() {
        instance = new Network();
        String[] arg = {""};
        instance.main(arg);

        TreeMap<String, User> users = new TreeMap<>();
        AdjacencyMatrixGraph<String, Double> relationships = new AdjacencyMatrixGraph<>();
        TreeMap<String, Country> countries = new TreeMap<>();
        Graph<String, String> borders = new Graph<>(false);

        users.put("u1", new User("u1", 25, "lisboa"));
        users.put("u2", new User("u2", 25, "madrid"));
        users.put("u3", new User("u3", 25, "paris"));
        users.put("u4", new User("u4", 25, "lisboa"));
        users.put("u5", new User("u5", 25, "lisboa"));
        users.put("u6", new User("u6", 25, "madrid"));
        users.put("u7", new User("u7", 25, "lisboa"));
        users.put("u8", new User("u8", 25, "lisboa"));
        users.put("u9", new User("u9", 25, "lisboa"));

        for (String key : users.keySet()) {
            relationships.insertVertex(key);
        }

        // u1 -> 7 || u2 -> 6 || u3 -> 5 || u4 -> 4 || u5 -> 4 || u6 -> 3 || u7 -> 2 || u8 -> 2 || u9 -> 1
        relationships.insertEdge("u1", "u2", 1D);
        relationships.insertEdge("u1", "u3", 1D);
        relationships.insertEdge("u1", "u4", 1D);
        relationships.insertEdge("u1", "u5", 1D);
        relationships.insertEdge("u1", "u6", 1D);
        relationships.insertEdge("u1", "u7", 1D);
        relationships.insertEdge("u1", "u8", 1D);
        relationships.insertEdge("u2", "u3", 1D);
        relationships.insertEdge("u2", "u4", 1D);
        relationships.insertEdge("u2", "u5", 1D);
        relationships.insertEdge("u2", "u6", 1D);
        relationships.insertEdge("u2", "u7", 1D);
        relationships.insertEdge("u3", "u4", 1D);
        relationships.insertEdge("u3", "u5", 1D);
        relationships.insertEdge("u3", "u6", 1D);
        relationships.insertEdge("u4", "u5", 1D);
        relationships.insertEdge("u8", "u9", 1D);

        Country portugal = new Country("portugal", "europa", 10.31F, "lisboa", 38.7071631F, -9.135517F);
        Country spain = new Country("espanha", "europa", 46.53F, "madrid", 40.4166909F, -3.7003454F);
        Country france = new Country("franca", "europa", 66.99F, "paris", 48.8566667F, 2.3509871F);
        countries.put("portugal", portugal);
        countries.put("espanha", spain);
        countries.put("franca", france);

        for (String key : countries.keySet()) {
            borders.insertVertex(key);
        }
        Double dSP = Country.calcularDistancia(spain, portugal);
        Double dSF = Country.calcularDistancia(spain, france);
        borders.insertEdge(portugal.getCountry(), spain.getCountry(), null, dSP);
        borders.insertEdge(france.getCountry(), spain.getCountry(), null, dSF);

        myNetwork = new reader(users, relationships, countries, borders);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getMostPopularUsers method, of class Network.
     */
    @Test
    public void testGetMostPopularUsers() {
        System.out.println("getMostPopularUsers");

        SortedMap<AbstractMap.SimpleEntry<String, Integer>, Integer> expResult = new TreeMap<AbstractMap.SimpleEntry<String, Integer>, Integer>((o1, o2) -> {
            return o2.getValue().compareTo(o1.getValue());
        });

        // u1 -> 7 || u2 -> 6 || u3 -> 5 || u4 -> 4 || u5 -> 4 || u6 -> 3 || u7 -> 2 || u8 -> 2 || u9 -> 1
        // u1
        int nUtilizadores = 1;
        expResult.put(new AbstractMap.SimpleEntry<>("u1", 7), 1);
        SortedMap<AbstractMap.SimpleEntry<String, Integer>, Integer> result = Network.getMostPopularUsers(myNetwork.getRelationships(), nUtilizadores);
        Iterator<AbstractMap.SimpleEntry<String, Integer>> itExp = expResult.keySet().iterator();
        Iterator<AbstractMap.SimpleEntry<String, Integer>> itRes = result.keySet().iterator();
        while (itExp.hasNext()) {
            assertTrue(itExp.next().equals(itRes.next()));
        }
        // u1 | u2
        nUtilizadores = 2;
        expResult.put(new AbstractMap.SimpleEntry<>("u2", 6), 1);
        result = Network.getMostPopularUsers(myNetwork.getRelationships(), nUtilizadores);
        itExp = expResult.keySet().iterator();
        itRes = result.keySet().iterator();
        while (itExp.hasNext()) {
            assertTrue(itExp.next().equals(itRes.next()));
        }
        // u1 | u2 | u3 | u4
        nUtilizadores = 4;
        expResult.put(new AbstractMap.SimpleEntry<>("u3", 5), 1);
        AbstractMap.SimpleEntry<String, Integer> u4 = new AbstractMap.SimpleEntry<>("u4", 4);
        expResult.put(u4, 1);
        result = Network.getMostPopularUsers(myNetwork.getRelationships(), nUtilizadores);
        itExp = expResult.keySet().iterator();
        itRes = result.keySet().iterator();
        while (itExp.hasNext()) {
            assertTrue(itExp.next().equals(itRes.next()));
        }
        // Verificar se em vez de u4 a lista retorna u5 -> Deve de retornar u4 e não substituir por u5.
        expResult.remove(u4); // Apesar de u4 e u5 terem o mesmo nº de amigos, u4 é encontrado 1º
        expResult.put(new AbstractMap.SimpleEntry<>("u5", 4), 1);
        itExp = expResult.keySet().iterator();
        itRes = result.keySet().iterator();
        assertTrue(itExp.next().equals(itRes.next())); //u1 == u1
        assertTrue(itExp.next().equals(itRes.next())); //u2 == u2
        assertTrue(itExp.next().equals(itRes.next())); //u3 == u3
        assertFalse(itExp.next().equals(itRes.next())); //u5 != u4
    }

    /**
     * Test of ex02 method, of class Network.
     */
    @Test
    public void testEx02() {
        System.out.println("ex02");

        ArrayList<String> result = new ArrayList<>();
        ArrayList<String> expResult = new ArrayList<>();

        expResult = new ArrayList<>(Arrays.asList("u2", "u3", "u4", "u5", "u6", "u7", "u8"));
        result = Network.ex02(myNetwork.getRelationships(), 1);
        assertTrue(expResult.equals(result));

        expResult = new ArrayList<>(Arrays.asList("u3", "u4", "u5", "u6", "u7"));
        result = Network.ex02(myNetwork.getRelationships(), 2);
        assertTrue(expResult.equals(result));

        expResult = new ArrayList<>(Arrays.asList("u5"));
        result = Network.ex02(myNetwork.getRelationships(), 4);
        assertTrue(expResult.equals(result));

        System.out.println("\nex02 Time");
        long startTime = 0, stopTime = 0, elapsedTime = 0;
        int[] values = {1, 2, 3, 4, 5};
        for (int i = 0; i < values.length; i++) {
            startTime = System.currentTimeMillis();
            Network.ex02(instance.getR().getRelationships(), values[i]);
            stopTime = System.currentTimeMillis();
            elapsedTime = stopTime - startTime;
            System.out.printf("ElapsedTime %d cases: %d ms\n", values[i], elapsedTime);
        }

    }

    /**
     * Test of isConnected method, of class Network.
     */
    @Test
    public void testIsConnected() {
        System.out.println("isConnected");

        boolean expResult = false;
        boolean result = Network.isConnected(myNetwork.getRelationships(), "u0");
        assertTrue("1 - Vertice not found.", expResult == result);

        expResult = true;
        result = Network.isConnected(myNetwork.getRelationships(), "u1");
        assertTrue("2 - Graph shoul be connected.", expResult == result);

        AdjacencyMatrixGraph<String, Double> myGraph1 = new AdjacencyMatrixGraph<>();
        for (String key : myNetwork.getUsers().keySet()) {
            myGraph1.insertVertex(key);
        }
        myGraph1.insertEdge("u1", "u2", 1D);
        myGraph1.insertEdge("u1", "u3", 1D);
        myGraph1.insertEdge("u1", "u4", 1D);

        expResult = false;
        result = Network.isConnected(myGraph1, "u1");
        assertTrue("3 - Graph shouln't be connected.", expResult == result);

        myGraph1.insertEdge("u2", "u5", 1D);
        myGraph1.insertEdge("u2", "u6", 1D);
        myGraph1.insertEdge("u3", "u7", 1D);
        myGraph1.insertEdge("u3", "u8", 1D);
        myGraph1.insertEdge("u4", "u9", 1D);

        expResult = true;
        result = Network.isConnected(myGraph1, "u1");
        assertTrue("4 - Graph shoul be connected.", expResult == result);
    }

    /**
     * Test of ex03 method, of class Network.
     */
    @Test
    public void testEx03() {
        System.out.println("ex03");
        Integer expResult;
        Integer result;

        TreeMap<String, User> myUsers = (TreeMap<String, User>) myNetwork.getUsers().clone();
        AdjacencyMatrixGraph<String, Double> myRelationships = (AdjacencyMatrixGraph<String, Double>) myNetwork.getRelationships().clone();
        result = Network.ex03(myRelationships, myUsers);
        assertTrue("Connections should be 8 but it was " + result, result == 8);

        myRelationships.removeEdge("u8", "u1");
        result = Network.ex03(myRelationships, myUsers);
        assertTrue("Should return <null> but it was " + result, result == null);
    }

    /**
     * Test of getBorders method, of class Network.
     */
    @Test
    public void testGetBorders() {
        System.out.println("getBorders");
        User u = new User("u1", 25, "lisboa");
        int nFronteiras = 1;
        TreeMap<String, String> expResult = new TreeMap<>();
        expResult.put("portugal", "lisboa");
        expResult.put("espanha", "madrid");
        TreeMap<String, String> result = Network.getBorders(myNetwork.getCountries(), myNetwork.getBorders(), u, nFronteiras);

        Iterator<String> itExp = expResult.keySet().iterator();
        Iterator<String> itRes = result.keySet().iterator();
        while (itExp.hasNext()) {
            assertTrue(itExp.next().equals(itRes.next()));
        }

        nFronteiras = 2;
        expResult.put("franca", "paris");
        result = Network.getBorders(myNetwork.getCountries(), myNetwork.getBorders(), u, nFronteiras);
        itExp = expResult.keySet().iterator();
        itRes = result.keySet().iterator();
        while (itExp.hasNext()) {
            assertTrue(itExp.next().equals(itRes.next()));
        }

        nFronteiras = 0;
        result = Network.getBorders(myNetwork.getCountries(), myNetwork.getBorders(), u, nFronteiras);
        assertTrue(result == null);
    }

    /**
     * Test of ex04 method, of class Network.
     */
    @Test
    public void testEx04() {
        System.out.println("ex04");
        String utilizador;
        int nFronteiras;
        TreeMap<String, String[]> expResult = new TreeMap<String, String[]>();
        TreeMap<String, String[]> result;

        nFronteiras = 1;
        utilizador = "inexistente";
        result = Network.ex04(myNetwork.getCountries(), myNetwork.getBorders(), myNetwork.getUsers(), myNetwork.getRelationships(), utilizador, nFronteiras);
        assertTrue("Should return <null> but it was " + result, result == null);

        utilizador = "u1";
        result = Network.ex04(myNetwork.getCountries(), myNetwork.getBorders(), myNetwork.getUsers(), myNetwork.getRelationships(), utilizador, nFronteiras);
        String[] pFriends = {"u4", "u5", "u7", "u8"}; // amigos em lisboa
        String[] sFriends = {"u2", "u6"}; // amigos em madrid
        assertTrue("Size should be 2 but it was " + result.size(), result.size() == 2);
        assertTrue("First border should be lisboa but it was " + result.firstKey(), result.firstKey().equals("lisboa"));
        assertTrue("First border friends should be <" + Arrays.toString(pFriends) + "> but it was " + Arrays.toString(result.firstEntry().getValue()), 
                Arrays.equals(result.firstEntry().getValue(), pFriends));
        assertTrue("Second border should be madris", result.containsKey("madrid"));
        assertTrue("Second border friends should be <" + Arrays.toString(sFriends) + "> but it was " + Arrays.toString(result.get("madrid")), 
                Arrays.equals(result.get("madrid"), sFriends));
        
    }

    /**
     * Test of getCitiesByUserPercetage method, of class Network.
     */
    @Test
    public void testGetCitiesByUserPercetage() {
        System.out.println("getCitiesByUserPercetage");

        float pUtilizadores = 50;
        TreeMap<String, Float> expResult = new TreeMap<>();
        float pPortugal = 6 * 100 / 9;
        expResult.put("portugal", pPortugal);
        TreeMap<String, Float> result = Network.getCitiesByUserPercetage(myNetwork.getCountries(), myNetwork.getUsers(), pUtilizadores);
        assertTrue("Size of Map should be 1.", result.size() == 1);

        float pSpain = 2 * 100 / 9;
        expResult.put("espanha", pSpain);
        pUtilizadores = 20;
        result = Network.getCitiesByUserPercetage(myNetwork.getCountries(), myNetwork.getUsers(), pUtilizadores);
        assertTrue("Size of Map should be 2 but it was " + result.size() + ".", result.size() == 2);
        Iterator<Map.Entry<String, Float>> itRes = result.entrySet().iterator();
        while (itRes.hasNext()) {
            Map.Entry<String, Float> key = itRes.next();
            assertTrue(key.getKey() + " shouldn't be on Map.", expResult.containsKey(key.getKey()));
        }

        float pFrance = 1 * 100 / 9;
        expResult.put("franca", pFrance);
        pUtilizadores = 1;
        result = Network.getCitiesByUserPercetage(myNetwork.getCountries(), myNetwork.getUsers(), pUtilizadores);
        assertTrue("Size of Map should be 1 but it was " + result.size() + ".", result.size() == 3);
    }

    /**
     * Test of getAverageDistance method, of class Network.
     */
    @Test
    public void testGetAverageDistance() {
        System.out.println("getAverageDistance");
        int nCidades = 0;
        float pUtilizadores = 0.0F;
        TreeMap<String, Double> expResult = new TreeMap<>();
        TreeMap<String, Double> result;

        nCidades = 3;
        pUtilizadores = 50;
        result = Network.getAverageDistance(myNetwork.getCountries(), myNetwork.getUsers(), myNetwork.getBorders(), nCidades, pUtilizadores);
        assertTrue("Size of Map should be 1 but it was " + result.size(), result.size() == 1);
        assertTrue("Only country should be portugal but it was " + result.firstKey(), result.containsKey("portugal"));

        Double dPS = Country.calcularDistancia(myNetwork.getCountries().get("portugal"), myNetwork.getCountries().get("espanha"));
        Double dPF = Country.calcularDistancia(myNetwork.getCountries().get("portugal"), myNetwork.getCountries().get("franca"));
        Double mPortugal = (dPS + dPF) / 2;
        expResult.put("portugal", mPortugal);
        assertTrue("Média de portugal should be " + mPortugal + " but it was " + result.get("portugal"), result.get("portugal").equals(expResult.get("portugal")));

        nCidades = 1;
        pUtilizadores = 1;
        result = Network.getAverageDistance(myNetwork.getCountries(), myNetwork.getUsers(), myNetwork.getBorders(), nCidades, pUtilizadores);
        assertTrue("Only country should be espanha but it was " + result.firstKey(), result.containsKey("espanha"));
    }

    /**
     * Test of ex05 method, of class Network.
     */
    @Test
    public void testEx05() {
        System.out.println("ex05");
        int nCidades = 0;
        float pUtilizadores = 0.0F;
        TreeMap<String, Double> expResult = new TreeMap<>();
        TreeMap<String, Double> result;

        nCidades = 1;
        pUtilizadores = 50;
        result = Network.ex05(myNetwork.getCountries(), myNetwork.getUsers(), myNetwork.getBorders(), nCidades, pUtilizadores);
        assertTrue("Size should be 1 but it was " + result.size(), result.size() == 1);
        assertTrue("Country should be portugal but it was " + result.firstKey(), result.firstKey().equals("portugal"));

        Country alemanha = new Country("alemanha", "europa", 82.8F, "berlim", 52.5234051F, 13.4113999F);
        Country austria = new Country("austria", "europa", 8.77F, "viena", 48.2092062F, 16.3727778F);
        Country belgica = new Country("belgica", "europa", 11.37F, "bruxelas", 50.8462807F, 4.3547273F);
        myNetwork.getCountries().put("alemanha", alemanha);
        myNetwork.getCountries().put("austria", austria);
        myNetwork.getCountries().put("belgica", belgica);

        myNetwork.getBorders().insertVertex("alemanha");
        myNetwork.getBorders().insertVertex("austria");
        myNetwork.getBorders().insertVertex("belgica");
        Double dFA = Country.calcularDistancia(myNetwork.getCountries().get("franca"), alemanha);
        Double dAA = Country.calcularDistancia(alemanha, austria);
        Double dAB = Country.calcularDistancia(alemanha, belgica);
        myNetwork.getBorders().insertEdge("franca", alemanha.getCountry(), null, dFA);
        myNetwork.getBorders().insertEdge(alemanha.getCountry(), austria.getCountry(), null, dAA);
        myNetwork.getBorders().insertEdge(alemanha.getCountry(), belgica.getCountry(), null, dAB);

        nCidades = 1;
        pUtilizadores = 20;
        result = Network.ex05(myNetwork.getCountries(), myNetwork.getUsers(), myNetwork.getBorders(), nCidades, pUtilizadores);
        assertTrue("Size should be 1 but it was " + result.size(), result.size() == 1);
        assertTrue("Country should be espanha but it was " + result.firstKey(), result.firstKey().equals("espanha"));

        nCidades = 2;
        pUtilizadores = 10;
        result = Network.ex05(myNetwork.getCountries(), myNetwork.getUsers(), myNetwork.getBorders(), nCidades, pUtilizadores);
        assertTrue("Size should be 2 but it was " + result.size(), result.size() == 2);
        Iterator<Map.Entry<String, Double>> itRes = result.entrySet().iterator();
        expResult.put("espanha", 0D);
        expResult.put("franca", 0D);
        while (itRes.hasNext()) {
            Map.Entry<String, Double> key = itRes.next();
            assertTrue(key.getKey() + " shouldn't be on Map.", expResult.containsKey(key.getKey()));
        }
    }

    /**
     * Test of ex06 method, of class Network.
     */
    @Test
    public void testEx06() {
        System.out.println("ex06");
        String vOrig = "u1";
        String vDest = "u3";
        int numCidades = 0;
        Double minWeight = null;
        
        LinkedList<String> shortPath = new LinkedList<String>();
        
        numCidades = 1;
        minWeight = Network.ex06(myNetwork.getCountries(), myNetwork.getUsers(), myNetwork.getBorders(),myNetwork.getRelationships(), vOrig, vDest, numCidades, shortPath);
        assertTrue("Size should be 3 but it was " + shortPath.size(), shortPath.size() == 3);
        
    }

}
