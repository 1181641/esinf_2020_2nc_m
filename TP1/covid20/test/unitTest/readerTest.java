/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unitTest;

import csv.reader;
import csv.structure;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import static org.junit.Assert.assertTrue;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author tiagosantos
 */
@FixMethodOrder(MethodSorters.JVM)
public class readerTest {

    private static reader r;

    public readerTest() {
        File pathFile = new File(System.getProperty("user.dir"));
        pathFile = new File(pathFile, "src");
        pathFile = new File(pathFile, "csv");
        pathFile = new File(pathFile, "owid-covid-data.csv");

        try {
            r = new reader(pathFile);

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.UK);
            r.getLst().add(new csv.structure("AAA", "NOVO CONTINENTE", "NOVO LOCAL", formatter.parse("2020-01-01 23:59:59"), 99999, 99999, 0, 0, 0, 0, 0, 0.0, 0.0, 0.0, 50.0, 50.0, 0.0, 0.0));
            r.getLst().add(new csv.structure("AAA", "NOVO CONTINENTE", "NOVO LOCAL", formatter.parse("2020-01-02 23:59:59"), 100000, 1, 0, 0, 0, 0, 0, 0.0, 0.0, 0.0, 50.0, 50.0, 0.0, 0.0));
            r.getLst().add(new csv.structure("AAA", "NOVO CONTINENTE", "NOVO LOCAL", formatter.parse("2020-01-03 23:59:59"), 100500, 500, 0, 0, 0, 0, 0, 0.0, 0.0, 0.0, 50.0, 50.0, 0.0, 0.0));
            r.getLst().add(new csv.structure("AAA", "NOVO CONTINENTE", "NOVO LOCAL", formatter.parse("2020-01-04 23:59:59"), 100500, 0, 0, 0, 0, 0, 0, 0.0, 0.0, 0.0, 50.0, 50.0, 0.0, 0.0));
            r.getLst().add(new csv.structure("AAA", "NOVO CONTINENTE", "NOVO LOCAL", formatter.parse("2020-01-05 23:59:59"), 101467, 967, 0, 0, 0, 0, 0, 0.0, 0.0, 0.0, 50.0, 50.0, 0.0, 0.0));
        } catch (FileNotFoundException e) {
            assertTrue("Erro", false);
        } catch (ParseException e) {
            assertTrue("Erro na conversão de dados.", false);
        }
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testReaderFileNotFound() {
        //System.out.println("1º testReaderFileNotFound");
        File pathFile = new File(System.getProperty("user.dir"));
        pathFile = new File(pathFile, "src");
        pathFile = new File(pathFile, "csv");
        pathFile = new File(pathFile, "ficheiro-nao-existe.csv");

        try {
            reader read = new reader(pathFile);
            assertTrue("ficheiro existe", false);
        } catch (FileNotFoundException e) {
            assertTrue(true);
        } catch (ParseException e) {
            assertTrue("Erro na conversão de dados.", false);
        }
    }

    @Test
    public void testReaderParseError() {
        //System.out.println("2º testReaderParseError");
        File pathFile = new File(System.getProperty("user.dir"));
        pathFile = new File(pathFile, "src");
        pathFile = new File(pathFile, "csv");
        pathFile = new File(pathFile, "ficheiro-com-erros.csv");

        try {
            reader read = new reader(pathFile);
            assertTrue("Ficheiro sem erros de conversão.", false);
        } catch (FileNotFoundException e) {
            assertTrue("Ficheiro não existe", false);
        } catch (ParseException e) {
            assertTrue(true);
        }
    }

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 /* - - - - - - - - - - - - - - -GetTotalCasesFromCountry - - - - - - - - - - -*/
 /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    @Test
    public void testGetTotalCasesFromCountry() {
        //System.out.println("3.1º testGetTotalCasesFromCountry");
        List<structure.casesFromCountry> result = r.getTotalCasesFromCountry(100000);

        structure.casesFromCountry first = result.get(0);

        assertTrue((first.getIso_code() == "AAA"));
    }

    @Test
    public void testGetTotalCasesFromCountryV2() {
        //System.out.println("3.1º testGetTotalCasesFromCountryV2");
        List<structure.casesFromCountry> result = r.getTotalCasesFromCountryV2(100000);

        structure.casesFromCountry first = result.get(0);

        assertTrue((first.getIso_code() == "AAA"));
    }

    @Test
    public void testTimeGetTotalCasesFromCountry() {
        System.out.println("\n3.2º testGetTotalCasesFromCountry Time");

        long startTime = 0, stopTime = 0, elapsedTime = 0;
        List<structure.casesFromCountry> lst;
        int[] values = {500, 1000, 5000, 10000, 25000, 50000, 100000, 750000};
        for (int i = 0; i < values.length; i++) {
            startTime = System.currentTimeMillis();
            lst = r.getTotalCasesFromCountry(values[i]);
            stopTime = System.currentTimeMillis();
            elapsedTime = stopTime - startTime;
            System.out.printf("ElapsedTime %d cases: %d ms\n", values[i], elapsedTime);
        }

        System.out.println("\n3.2º testGetTotalCasesFromCountryV2 Time");
        for (int i = 0; i < values.length; i++) {
            startTime = System.currentTimeMillis();
            lst = r.getTotalCasesFromCountryV2(values[i]);
            stopTime = System.currentTimeMillis();
            elapsedTime = stopTime - startTime;
            System.out.printf("ElapsedTime %d cases: %d ms\n", values[i], elapsedTime);
        }
    }

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

 /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 /* - - - - - - - - - - - - - - GetNewCases - - - - - - - - - - - - - - - - - */
 /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    @Test
    public void testGetNewCases() {
        //System.out.println("4.1º testGetNewCases");
        List<structure.casesByMonth> result = null;
        try {
            result = r.getNewCases();

            if (result.isEmpty()) {
                assertTrue("Não existe dados retornados", false);
            }

        } catch (Exception e) {
            assertTrue("Erro", false);
        } finally {
            if (result == null) {
                result = new ArrayList<>();
            }
        }

        assertTrue(result.stream().anyMatch(p -> p.getContinent().equals("NOVO CONTINENTE")
                && p.getMonth() == 1
                && p.getNew_cases() == 101467));
    }

    @Test
    public void testTimeGetNewCases() {
        System.out.println("\n4.2º testGetNewCases Time");
        long startTime = 0, stopTime = 0, elapsedTime = 0;
        List<structure.casesByMonth> lst;

        startTime = System.currentTimeMillis();
        lst = r.getNewCases();
        stopTime = System.currentTimeMillis();
        elapsedTime = stopTime - startTime;
        System.out.printf("ElapsedTime: %d ms\n", elapsedTime);
    }

 /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    
 /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 /* - - - - - - - - - GetNewCasesByDayFromContinentMonth - - - - - - - - - - - */
 /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    @Test
    public void testGetNewCasesByDayFromContinentMonth() {
        //System.out.println("5.1º testGetNewCasesByDayFromContinentMonth");
        List<structure.casesByDay> result = r.getNewCasesByDayFromContinentMonth(1, "NOVO CONTINENTE");

        assertTrue(result.stream().anyMatch(p -> p.getDay() == 3 && p.getLocation().equals("NOVO LOCAL") && p.getNew_cases() == 500));
    }

    @Test
    public void testTimeGetNewCasesByDayFromContinentMonth() {
        System.out.println("\n5.2º testGetNewCasesByDayFromContinentMonth Time");
        List<structure.casesByDay> result;

        long startTime = 0, stopTime = 0, elapsedTime = 0;
        List<structure.casesByDay> lst;
        int[] values = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
        for (int i = 0; i < values.length; i++) {
            startTime = System.currentTimeMillis();
            lst = r.getNewCasesByDayFromContinentMonth(values[i], "Europe");
            stopTime = System.currentTimeMillis();
            elapsedTime = stopTime - startTime;
            System.out.printf("ElapsedTime month %d: %d ms\n", values[i], elapsedTime);
        }
    }

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    
 /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 /* - - - - - - - - - - - - GetNewDeathsBySmokers - - - - - - - - - - - - - - - */
 /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    @Test
    public void testGetNewDeathsBySmokers() {
        //System.out.println("6.1º testGetNewDeathsBySmokers");

        List<structure.deathsBySmokers> result = r.getNewDeathsBySmokers(99.99);

        assertTrue(result.stream().anyMatch(p -> p.getCountry().equals("NOVO LOCAL") && p.getSmokers_percentage() == 100.0));
    }

    @Test
    public void testGetNewDeathsBySmokersV2() {
        //System.out.println("6.1º testGetNewDeathsBySmokersV2");

        List<structure.deathsBySmokers> result = r.getNewDeathsBySmokersV2(99.99);

        assertTrue(result.stream().anyMatch(p -> p.getCountry().equals("NOVO LOCAL") && p.getSmokers_percentage() == 100.0));
    }

    @Test
    public void testTimeGetNewDeathsBySmokers() {
        System.out.println("\n6.2º testGetNewDeathsBySmokers Time");
        long startTime = 0, stopTime = 0, elapsedTime = 0;
        List<structure.deathsBySmokers> lst;
        double[] values = {1.0, 5.0, 10.0, 20.0, 50.0, 70};
        for (int i = 0; i < values.length; i++) {
            startTime = System.currentTimeMillis();
            lst = r.getNewDeathsBySmokers(values[i]);
            stopTime = System.currentTimeMillis();
            elapsedTime = stopTime - startTime;
            System.out.printf("ElapsedTime %.1f smokers: %d ms\n", values[i], elapsedTime);
        }

        System.out.println("\n6.2º testGetNewDeathsBySmokersV2 Time");
        for (int i = 0; i < values.length; i++) {
            startTime = System.currentTimeMillis();
            lst = r.getNewDeathsBySmokersV2(values[i]);
            stopTime = System.currentTimeMillis();
            elapsedTime = stopTime - startTime;
            System.out.printf("ElapsedTime %.1f smokers: %d ms\n", values[i], elapsedTime);
        }
    }


 /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
}
