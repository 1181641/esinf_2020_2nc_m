/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csv;

import java.util.Comparator;
import java.util.Date;

/**
 *
 * @author (1181312) Hugo Costa | (1181641) Tiago Santos
 */
public class structure {

    private String iso_code;
    private String continent;
    private String location;
    
    private Date date;
    
    private Integer total_cases;
    private Integer new_cases;
    private Integer total_deaths;
    private Integer new_deaths;
    private Integer new_tests;
    private Integer total_tests;
    private Integer population;
    
    private Double aged_65_older;
    private Double cardiovasc_death_rate;
    private Double diabetes_prevalence;
    private Double female_smokers;
    private Double male_smokers;
    private Double hospital_beds_per_thousand;
    private Double life_expectancy;

    public String getIso_code() {
        return iso_code;
    }

    public void setIso_code(String iso_code) {
        this.iso_code = iso_code;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getTotal_cases() {
        return total_cases;
    }

    public void setTotal_cases(Integer total_cases) {
        this.total_cases = total_cases;
    }

    public Integer getNew_cases() {
        return new_cases;
    }

    public void setNew_cases(Integer new_cases) {
        this.new_cases = new_cases;
    }

    public Integer getTotal_deaths() {
        return total_deaths;
    }

    public void setTotal_deaths(Integer total_deaths) {
        this.total_deaths = total_deaths;
    }

    public Integer getNew_deaths() {
        return new_deaths;
    }

    public void setNew_deaths(Integer new_deaths) {
        this.new_deaths = new_deaths;
    }

    public Integer getNew_tests() {
        return new_tests;
    }

    public void setNew_tests(Integer new_tests) {
        this.new_tests = new_tests;
    }

    public Integer getTotal_tests() {
        return total_tests;
    }

    public void setTotal_tests(Integer total_tests) {
        this.total_tests = total_tests;
    }

    public Integer getPopulation() {
        return population;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }

    public Double getAged_65_older() {
        return aged_65_older;
    }

    public void setAged_65_older(Double aged_65_older) {
        this.aged_65_older = aged_65_older;
    }

    public Double getCardiovasc_death_rate() {
        return cardiovasc_death_rate;
    }

    public void setCardiovasc_death_rate(Double cardiovasc_death_rate) {
        this.cardiovasc_death_rate = cardiovasc_death_rate;
    }

    public Double getDiabetes_prevalence() {
        return diabetes_prevalence;
    }

    public void setDiabetes_prevalence(Double diabetes_prevalence) {
        this.diabetes_prevalence = diabetes_prevalence;
    }

    public Double getFemale_smokers() {
        return female_smokers;
    }

    public void setFemale_smokers(Double female_smokers) {
        this.female_smokers = female_smokers;
    }

    public Double getMale_smokers() {
        return male_smokers;
    }

    public void setMale_smokers(Double male_smokers) {
        this.male_smokers = male_smokers;
    }

    public Double getHospital_beds_per_thousand() {
        return hospital_beds_per_thousand;
    }

    public void setHospital_beds_per_thousand(Double hospital_beds_per_thousand) {
        this.hospital_beds_per_thousand = hospital_beds_per_thousand;
    }

    public Double getLife_expectancy() {
        return life_expectancy;
    }

    public void setLife_expectancy(Double life_expectancy) {
        this.life_expectancy = life_expectancy;
    }
    
    public structure() {
    }

    public structure(String iso_code, String continent, String location, Date date, Integer total_cases, Integer new_cases, Integer total_deaths, Integer new_deaths, Integer new_tests, Integer total_tests, Integer population, Double aged_65_older, Double cardiovasc_death_rate, Double diabetes_prevalence, Double female_smokers, Double male_smokers, Double hospital_beds_per_thousand, Double life_expectancy) {
        this.iso_code = iso_code;
        this.continent = continent;
        this.location = location;
        this.date = date;
        this.total_cases = total_cases;
        this.new_cases = new_cases;
        this.total_deaths = total_deaths;
        this.new_deaths = new_deaths;
        this.new_tests = new_tests;
        this.total_tests = total_tests;
        this.population = population;
        this.aged_65_older = aged_65_older;
        this.cardiovasc_death_rate = cardiovasc_death_rate;
        this.diabetes_prevalence = diabetes_prevalence;
        this.female_smokers = female_smokers;
        this.male_smokers = male_smokers;
        this.hospital_beds_per_thousand = hospital_beds_per_thousand;
        this.life_expectancy = life_expectancy;
    }
    
    public static class casesFromCountry{

        public String getIso_code() {
            return iso_code;
        }

        public void setIso_code(String iso_code) {
            this.iso_code = iso_code;
        }

        public String getContinent() {
            return continent;
        }

        public void setContinent(String continent) {
            this.continent = continent;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public Integer getTotal_cases() {
            return total_cases;
        }

        public void setTotal_cases(Integer total_cases) {
            this.total_cases = total_cases;
        }

        public Integer getMindays() {
            return mindays;
        }

        public void setMindays(Integer mindays) {
            this.mindays = mindays;
        }
        
        private String iso_code;
        private String continent;
        private String location;
        private Date date;
        private Integer total_cases;
        private Integer mindays;

        public casesFromCountry(){
        }
        
        
        
        public static Comparator<casesFromCountry> daysComparator = new Comparator<casesFromCountry>() {            
            public int compare(casesFromCountry c1, casesFromCountry c2) {
                int mindays1 = c1.getMindays();
                int mindays2 = c2.getMindays();

                return mindays1 - mindays2;
            }
        };
        
    }
    
    public static class casesByDay{
        private int day;
        private String location;
        private Integer new_cases;

        public int getDay() {
            return day;
        }

        public void setDay(int day) {
            this.day = day;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = (location == null ? "" : location);
        }

        public Integer getNew_cases() {
            return new_cases;
        }

        public void setNew_cases(Integer new_cases) {
            this.new_cases = (new_cases == null ? 0 : new_cases);
        }
        
        public casesByDay(){
        }
        
    }
    
    public static class casesByMonth {
        
        private String continent;
        private int month;
        private int new_cases;
        private int new_deaths;

        public String getContinent() {
            return continent;
        }

        public void setContinent(String continent) {
            this.continent = continent;
        }

        public int getMonth() {
            return month;
        }

        public void setMonth(int month) {
            this.month = month;
        }

        public int getNew_cases() {
            return new_cases;
        }

        public void setNew_cases(int new_cases) {
            this.new_cases = new_cases;
        }

        public int getNew_deaths() {
            return new_deaths;
        }

        public void setNew_deaths(int new_deaths) {
            this.new_deaths = new_deaths;
        }

        public casesByMonth(){}
        public casesByMonth(String continent, int month, int new_cases, int new_deaths) {
            this.continent = continent;
            this.month = month;
            this.new_cases = new_cases;
            this.new_deaths = new_deaths;
        }
        
        
        public static Comparator<casesByMonth> continentComparator = new Comparator<casesByMonth>() {            
            public int compare(casesByMonth c1, casesByMonth c2) {
                String continent1 = c1.getContinent().toUpperCase();
                String continent2 = c2.getContinent().toUpperCase();

                return continent1.compareTo(continent2);
            }
        };
        
        public static Comparator<casesByMonth> monthComparator = new Comparator<casesByMonth>() {            
            public int compare(casesByMonth c1, casesByMonth c2) {
                int month1 = c1.getMonth();
                int month2 = c2.getMonth();

                return month1 - month2;
            }
        };
        
    }
    
    public static class deathsBySmokers {
        
        private String country;
        private Double smokers_percentage;
        private int new_deaths;

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public Double getSmokers_percentage() {
            return smokers_percentage;
        }

        public void setSmokers_percentage(Double smokers_percentage) {
            this.smokers_percentage = smokers_percentage;
        }

        public int getNew_deaths() {
            return new_deaths;
        }

        public void setNew_deaths(int new_deaths) {
            this.new_deaths = new_deaths;
        }
        
        public deathsBySmokers(){}
        public deathsBySmokers(String country, Double smokers_percentage, int new_deaths) {
            this.country = country;
            this.smokers_percentage = smokers_percentage;
            this.new_deaths = new_deaths;
        }
        
        public static Comparator<deathsBySmokers> deathsComparator = new Comparator<deathsBySmokers>() {            
            public int compare(deathsBySmokers c1, deathsBySmokers c2) {
                int new_death1 = c1.getNew_deaths();
                int new_death2 = c2.getNew_deaths();

                return new_death2 - new_death1;
            }
        };
    }
}
