/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author (1181312) Hugo Costa | (1181641) Tiago Santos
 */
public class reader {

    private static ArrayList<structure> lst = new ArrayList<>();

    public static ArrayList<structure> getLst() {
        return lst;
    }

    public reader(File pathFile) throws FileNotFoundException, ParseException {

        String csvFile = pathFile.getPath();
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

            br = new BufferedReader(new FileReader(csvFile));
            // ignore 1º line - Header
            lst = new ArrayList<structure>();
            boolean isHeaderFirst = true;
            while ((line = br.readLine()) != null) {
                // use comma as separator
                String[] arrayData = line.replace("\"", "").split(",");
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.UK);

                if (!isHeaderFirst) {

                    structure data = new structure();
                    data.setIso_code(arrayData[0]);
                    data.setContinent(arrayData[1]);
                    data.setLocation(arrayData[2]);

                    data.setDate(formatter.parse(arrayData[3] + " 23:59:59"));

                    data.setTotal_cases((arrayData[4].equals("NA") ? 0 : Integer.parseInt(arrayData[4])));
                    data.setNew_cases((arrayData[5].equals("NA") ? 0 : Integer.parseInt(arrayData[5])));
                    data.setTotal_deaths((arrayData[6].equals("NA") ? 0 : Integer.parseInt(arrayData[6])));
                    data.setNew_deaths((arrayData[7].equals("NA") ? 0 : Integer.parseInt(arrayData[7])));
                    data.setNew_tests((arrayData[8].equals("NA") ? 0 : Integer.parseInt(arrayData[8])));
                    data.setTotal_tests((arrayData[9].equals("NA") ? 0 : Integer.parseInt(arrayData[9])));
                    data.setPopulation((arrayData[10].equals("NA") ? 0 : Integer.parseInt(arrayData[10])));

                    data.setAged_65_older((arrayData[11].equals("NA") ? 0.0 : Double.parseDouble(arrayData[11])));
                    data.setCardiovasc_death_rate((arrayData[12].equals("NA") ? 0.0 : Double.parseDouble(arrayData[12])));
                    data.setDiabetes_prevalence((arrayData[13].equals("NA") ? 0.0 : Double.parseDouble(arrayData[13])));
                    data.setFemale_smokers((arrayData[14].equals("NA") ? 0.0 : Double.parseDouble(arrayData[14])));
                    data.setMale_smokers((arrayData[15].equals("NA") ? 0.0 : Double.parseDouble(arrayData[15])));
                    data.setHospital_beds_per_thousand((arrayData[16].equals("NA") ? 0.0 : Double.parseDouble(arrayData[16])));
                    data.setLife_expectancy((arrayData[17].equals("NA") ? 0.0 : Double.parseDouble(arrayData[17])));

                    lst.add(data);
                } else {
                    isHeaderFirst = false;
                }
            }

        } catch (ParseException e) {
            throw new ParseException("Ficheiro com erros", 0);
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("Ficheiro não Existe");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * [2] Função para returnar os países ordenados por número dias até atingir
     * "casesNumber" casos.Ordenado por número de dias crescente.
     *
     * @param casesNumber Número de casos pretendidos (Ponto 2 - 50.000).
     * @return Lista de valores ordenado por número de dias.
     */
    @Deprecated
    public List<structure.casesFromCountry> getTotalCasesFromCountry(int casesNumber) {
        ArrayList<structure> retValue = lst;

        Collections.sort(retValue, (o1, o2) -> {
            return o1.getDate().compareTo(o2.getDate()); //To change body of generated lambdas, choose Tools | Templates.
        });

        ArrayList<structure.casesFromCountry> arrayData = new ArrayList<>();

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.UK);
            Date firstDate = sdf.parse("01/01/2020");

            retValue.forEach((data) -> {
                
                if(data.getTotal_cases() < casesNumber)
                    return;
                

                structure.casesFromCountry newdata = new structure.casesFromCountry();
                Date dateFromData = data.getDate();
                

                long diffInMillies = Math.abs(dateFromData.getTime() - firstDate.getTime());
                Integer diff = (int) TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
                if (diffInMillies > 0 && diff == 0)
                    diff = 1;
                
                if (arrayData.stream().anyMatch(p -> p.getIso_code().equals(data.getIso_code()))) {
                    newdata = arrayData.stream().filter(p -> p.getIso_code().equals(data.getIso_code())).findFirst().get();
                    if (newdata.getTotal_cases() < casesNumber) {
                        newdata.setDate(data.getDate());
                        newdata.setTotal_cases(data.getTotal_cases());
                        newdata.setMindays(diff);
                    }
                } else {
                    newdata.setIso_code(data.getIso_code());
                    newdata.setContinent(data.getContinent());
                    newdata.setLocation(data.getLocation());
                    newdata.setDate(data.getDate());
                    newdata.setTotal_cases(data.getTotal_cases());
                    newdata.setMindays(diff);

                    arrayData.add(newdata);
                }
            });

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<structure.casesFromCountry> newLst = arrayData.stream()
                .filter(p -> p.getTotal_cases() != null && p.getTotal_cases() >= casesNumber)
                .sorted((o1, o2) -> {
                    return o1.getMindays().compareTo(o2.getMindays());
                }).collect(Collectors.toList());

        return newLst;
    }

    /**
     * [2] Função para returnar os países ordenados por número dias até atingir
     * "casesNumber" casos.Ordenado por número de dias crescente. Utilização de
     * TreeMap para tratamento de dados. Maior eficiência.
     *
     * @param casesNumber Número de casos pretendidos (Ponto 2 - 50.000).
     * @return Lista de valores ordenado por número de dias.
     */
    public List<structure.casesFromCountry> getTotalCasesFromCountryV2(int casesNumber) {
        List<structure.casesFromCountry> newLst = new ArrayList<>();
        try {
            TreeMap<String, structure.casesFromCountry> newMap = new TreeMap<>();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.UK);
            Date firstDate = sdf.parse("01/01/2020");
            long diffInMillies = 0;
            int diff = 0;

            for (csv.structure item : lst) {
                if (item.getTotal_cases() > casesNumber) {
                    diffInMillies = Math.abs(item.getDate().getTime() - firstDate.getTime());
                    diff = (int) TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

                    if (newMap.get(item.getIso_code()) != null) {
                        // Se número de dias é inferior, atualiza os valores
                        if (newMap.get(item.getIso_code()).getMindays() >= diff) {
                            newMap.get(item.getIso_code()).setDate(item.getDate());
                            newMap.get(item.getIso_code()).setTotal_cases(item.getTotal_cases());
                            newMap.get(item.getIso_code()).setMindays(diff);
                        }
                    } else {
                        // Adiciona a linha ao Map
                        structure.casesFromCountry newLin = new structure.casesFromCountry();
                        newLin.setIso_code(item.getIso_code());
                        newLin.setContinent(item.getContinent());
                        newLin.setLocation(item.getLocation());
                        newLin.setDate(item.getDate());
                        newLin.setTotal_cases(item.getTotal_cases());
                        newLin.setMindays(diff);

                        newMap.put(item.getIso_code(), newLin);
                    }
                }
            }

            for (String key : newMap.keySet()) {
                newLst.add(newMap.get(key));
            }
            Collections.sort(newLst, structure.casesFromCountry.daysComparator); // Ordenar por número de mortes decrescente

        } catch (ParseException ex) {
            Logger.getLogger(reader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return newLst;
    }

    /**
     * [3] Função para returnar os novos casos por Continente/Mês.Ordenado por
     * continente/mês crescente.
     *
     * @return Lista de valores ordenado por continente/mês.
     */
    public ArrayList<structure.casesByMonth> getNewCases() {
        ArrayList<structure.casesByMonth> newLst = new ArrayList<>();

        for (csv.structure item : lst) {
            boolean exist = false;

            Calendar cal = Calendar.getInstance();
            cal.setTime(item.getDate());
            int month = cal.get(Calendar.MONTH) + 1;

            int new_cases = 0;
            if (item.getNew_cases() != null) {
                new_cases = item.getNew_cases();
            }

            int new_deaths = 0;
            if (item.getNew_deaths() != null) {
                new_deaths = item.getNew_deaths();
            }

            for (structure.casesByMonth cases_item : newLst) {
                // Atualizar dados caso já exista
                if (cases_item.getContinent().equals(item.getContinent()) && cases_item.getMonth() == month) {
                    exist = true;
                    cases_item.setNew_cases(cases_item.getNew_cases() + new_cases);
                    cases_item.setNew_deaths(cases_item.getNew_deaths() + new_deaths);
                    break;
                }
            }

            if (!exist) {
                // Adicionar linha
                structure.casesByMonth newLin = new structure.casesByMonth(item.getContinent(), month, new_cases, new_deaths);
                newLst.add(newLin);
            }
        }

        Collections.sort(newLst, structure.casesByMonth.monthComparator); // Ordenar por mês
        Collections.sort(newLst, structure.casesByMonth.continentComparator); // Ordenar por continente

        return newLst;
    }

    /**
     * [4] Função para returnar os novos casos positivos para cada dia de um mês
     * de um continente.Ordenado por número de novos casos decrescente.
     *
     * @param mes número do mês ([1 - janeiro], [2 - fevereiro], [3 - março], [4
     * - abril], [5 - maio], [6 - junho], [7 - julho], [8 - agosto], [9 -
     * setembro], [10 - outubro], [11 - novembro], [12 - dezembro]).
     * @param continente número do continente (Conforme a ordem do ficheiro).
     * @return Lista de valores ordenado por número de novos casos.
     */  
    public List<structure.casesByDay> getNewCasesByDayFromContinentMonth(int mes, String continente) {
        List<structure.casesByDay> result = new ArrayList<>();
        LocalDate local;
        
        for (structure data : lst) {
            
            if ((data.getContinent() != null && data.getContinent().equals(continente))
                && (data.getDate() != null && data.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getMonthValue() == mes)){
            
                local = data.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                structure.casesByDay item = new structure.casesByDay();
                item.setDay(local.getDayOfMonth());
                item.setLocation(data.getLocation());
                item.setNew_cases(data.getNew_cases());
                result.add(item);
            }
            
        }
        
        Collections.sort(result, Comparator.comparing(structure.casesByDay::getDay)
            .thenComparing((o1, o2) -> {
                return o2.getNew_cases().compareTo(o1.getNew_cases()); //To change body of generated lambdas, choose Tools | Templates.
            })
            .thenComparing(structure.casesByDay::getLocation));
        
        return result;
        
    }
   
    /**
     * [5] Função para returnar as mortes por países cuja percentagem de
     * fumadores é superior a "smoker_percentage".Ordenado por número de mortes
     * decrescente.
     *
     * @param smoker_percentage Percentagem de fumadores pretendida (Ponto 5 -
     * 70%).
     * @return Lista de valores ordenado por número de mortes
     */
    @Deprecated
    public List<structure.deathsBySmokers> getNewDeathsBySmokers(double smoker_percentage) {
        List<structure.deathsBySmokers> newLst = new ArrayList<>();

        for (csv.structure item : lst) {
            if (item.getMale_smokers() != null && item.getFemale_smokers() != null) {
                // Verificar se percentagem de fumadores é superios a "smoker_percentage"
                if (item.getMale_smokers() + item.getFemale_smokers() > smoker_percentage) {
                    int new_deaths = (item.getNew_deaths() == null ? 0 : item.getNew_deaths());
                    boolean exist = false;

                    for (structure.deathsBySmokers cases_item : newLst) {
                        // Atualizar dados caso já exista
                        if (cases_item.getCountry().equals(item.getLocation())) {
                            exist = true;
                            cases_item.setNew_deaths(cases_item.getNew_deaths() + new_deaths);
                        }
                    }

                    if (!exist) {
                        // Adicionar linha
                        double smoke_percentage = item.getMale_smokers() + item.getFemale_smokers();
                        structure.deathsBySmokers newLin = new structure.deathsBySmokers(item.getLocation(), smoke_percentage, new_deaths);
                        newLst.add(newLin);
                    }
                }
            }
        }

        Collections.sort(newLst, structure.deathsBySmokers.deathsComparator); // Ordenar por número de mortes decrescente

        return newLst;
    }

    /**
     * [5] Função para returnar as mortes por países cuja percentagem de
     * fumadores é superior a "smoker_percentage".Ordenado por número de mortes
     * decrescente. Utilização de TreeMap para tratamento dos dados. Maior
     * eficiência.
     *
     * @param smoker_percentage Percentagem de fumadores pretendida (Ponto 5 -
     * 70%).
     * @return Lista de valores ordenado por número de mortes
     */
    public List<structure.deathsBySmokers> getNewDeathsBySmokersV2(double smoker_percentage) {
        List<structure.deathsBySmokers> newLst = new ArrayList<>();

        TreeMap<String, structure.deathsBySmokers> newMap = new TreeMap<>();
        for (csv.structure item : lst) {
            if (item.getMale_smokers() == null && item.getFemale_smokers() == null) {
                continue;
            }
            
            if (item.getMale_smokers() == 0.0 && item.getFemale_smokers() == 0.0) {
                continue;
            }

            if (item.getMale_smokers() + item.getFemale_smokers() > smoker_percentage) {
                int new_deaths = (item.getNew_deaths() == null ? 0 : item.getNew_deaths());

                if (newMap.get(item.getLocation()) != null) {
                    new_deaths += newMap.get(item.getLocation()).getNew_deaths();
                    newMap.get(item.getLocation()).setNew_deaths(new_deaths);
                } else {
                    double smoke_percentage = item.getMale_smokers() + item.getFemale_smokers();
                    structure.deathsBySmokers newLin = new structure.deathsBySmokers(item.getLocation(), smoke_percentage, new_deaths);
                    newMap.put(item.getLocation(), newLin);
                }
            }
            
        }

        for (String key : newMap.keySet()) {
            newLst.add(newMap.get(key));
        }
        Collections.sort(newLst, structure.deathsBySmokers.deathsComparator); // Ordenar por número de mortes decrescente

        return newLst;

    }

}
