/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package covid20;

import csv.reader;
import csv.structure;
import csv.structure.casesByDay;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author (1181312) Hugo Costa | (1181641) Tiago Santos
 */
public class covid20 {

    private static reader r;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try{
            File pathFile = new File(System.getProperty("user.dir"));
            pathFile = new File(pathFile, "src");
            pathFile = new File(pathFile, "csv");
            pathFile = new File(pathFile, "owid-covid-data.csv");

            r = new reader(pathFile);

            System.out.println("\n-------------------");
            System.out.println("----- PONTO 2 -----");
            System.out.println("-------------------");
            printTotalCasesFromCountry(50000);

            System.out.println("\n-------------------");
            System.out.println("----- PONTO 3 -----");
            System.out.println("-------------------");
            printNewCases();

            System.out.println("\n-------------------");
            System.out.println("----- PONTO 4 -----");
            System.out.println("-------------------");
            inputValueNewCasesByDayFromContinentMonth();

            System.out.println("\n-------------------");
            System.out.println("----- PONTO 5 -----");
            System.out.println("-------------------");
            printNewDeathsBySmokers(70.0);
            
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }catch(ParseException e){
            e.printStackTrace();
        }
        
    }

    private static void printNewCases() {
        ArrayList<structure.casesByMonth> newLst = r.getNewCases();

        // Print - Cabeçalho da tabela
        System.out.println("+------------------------------------------------------------+");
        System.out.println("|     Continent     +   Month  +   New cases  +  New deaths  |");
        System.out.println("+-------------------+----------+--------------+--------------+");

        // Print - Linhas da tabela
        newLst.forEach(item -> {
            System.out.printf("|  %-15s  +  %6d  +  %10d  +  %10d  |\n",
                    item.getContinent(), item.getMonth(), item.getNew_cases(), item.getNew_deaths());
        });
    }

    public static void printTotalCasesFromCountry(int casesNumber) {
        List<structure.casesFromCountry> newLst = r.getTotalCasesFromCountryV2(casesNumber);

        System.out.println("+---------------------------------------------------------------------------------------------+");
        System.out.println("|  ISO  +     Continent     +        Location         +     Date     + Total cases + Min days |");
        System.out.println("+-------+-------------------+-------------------------+--------------+-------------+----------+");
        
        newLst.forEach(data -> {
            SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
            int totalCases = (data.getTotal_cases() == null ? 0 : data.getTotal_cases());

            System.out.printf("| %5s +  %-15s  +  %-21s  +  %10s  +  %9d  +  %6d  |\n",
                    data.getIso_code(), data.getContinent(), data.getLocation(), dt1.format(data.getDate()), totalCases, data.getMindays());
        });
    }

    public static void inputValueNewCasesByDayFromContinentMonth() {
        BufferedReader reader
                = new BufferedReader(new InputStreamReader(System.in));
        String valueInput = "";
        Boolean isValid = false;

        Integer numMes = 0;
        Integer positionContinente = 0;

        // RECEBER O INPUT DO MÊS
        System.out.println("Indique um mês[1-12]:");
        while (!isValid) {
            try {
                valueInput = reader.readLine();
                numMes = Integer.parseInt(valueInput);
                if (numMes >= 1 && numMes <= 12) {
                    isValid = true;
                }
            } catch (IOException e) {
                isValid = false;
            } catch (NumberFormatException e) {
                isValid = false;
            }
        }

        List<String> continentes = r.getLst().stream().map(m -> m.getContinent()).distinct().collect(Collectors.toList());
        Integer indexContinente = 1;
        System.out.println("Indique o continente:");
        for (String data : continentes) {
            System.out.println(indexContinente.toString() + ": " + data);
            indexContinente++;
        }

        // RECEBER O  INPUT DO CONTINENTE
        reader = new BufferedReader(new InputStreamReader(System.in));
        isValid = false;
        while (!isValid) {
            try {
                valueInput = reader.readLine();
                positionContinente = Integer.parseInt(valueInput);
                if (positionContinente >= 1 && positionContinente <= (indexContinente - 1)) {
                    isValid = true;
                }
            } catch (IOException e) {
                isValid = false;
            } catch (NumberFormatException e) {
                isValid = false;
            }
        }

        printNewCasesByDayFromContinentMonth(numMes.intValue(), continentes.get(positionContinente.intValue() - 1));
    }

    public static void printNewCasesByDayFromContinentMonth(int mes, String continente) {
        List<structure.casesByDay> result = r.getNewCasesByDayFromContinentMonth(mes, continente);

        String prefix;
        int countDay = 0;
        for (casesByDay data : result) {

            prefix = "         ";
            if (countDay != data.getDay()) {
                System.out.println("");
                prefix = "Dia " + data.getDay() + " -->";
                countDay = data.getDay();
            }
            System.out.println(prefix + "  " + data.getLocation() + "(" + data.getNew_cases().toString() + ")");

        }
    }
    
    public static void printNewDeathsBySmokers(double smoker_percentage) {
        List<structure.deathsBySmokers> newLst = r.getNewDeathsBySmokersV2(smoker_percentage);
        // Print - Linhas
        newLst.forEach(item -> {
            System.out.printf("[%s, %.1f, %d]\n", item.getCountry(), item.getSmokers_percentage(), item.getNew_deaths());
        });
    }
}
